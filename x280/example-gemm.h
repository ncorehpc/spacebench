/* Copyright 2019 SiFive, Inc */
/* SPDX-License-Identifier: Apache-2.0 */
#ifndef GEMM_H
#define GEMM_H
#include <inttypes.h>
#include <string.h>

/* Datatype for matrix elements */
#define GEMM_TYPE   float

/* cblas_sgemm or cblas_dgemm (must match GEMM_TYPE) */
#define GEMM_FUNC   cblas_sgemm

/* CBLAS Layout Settings */
typedef enum CBLAS_LAYOUT {
    CblasRowMajor   = 101,
    CblasColMajor   = 102
} CBLAS_LAYOUT;

typedef enum CBLAS_TRANSPOSE {
    CblasNoTrans    = 111,  // trans='N'
    CblasTrans      = 112   // trans='T'
} CBLAS_TRANSPOSE;

#define IS_TRANS(t) ((t) != CblasNoTrans)
#define IS_COLMAJ(l) ((l) == CblasColMajor)

/* The leading dimension of a matrix, given its transposition (or lack) */
#define LDIMM(rows, cols, trans, layout) \
    ( \
      IS_COLMAJ(layout) ? \
        (IS_TRANS(trans) ? (cols) : (rows)) \
      : (IS_TRANS(trans) ? (rows) : (cols)) \
    )


#define RV_INT int64_t

/* GEMM APIs defined by CBLAS */

#define CBLAS_GEMM_ARGS             \
        CBLAS_LAYOUT        layout, \
        CBLAS_TRANSPOSE     transa, \
        CBLAS_TRANSPOSE     transb, \
        RV_INT              m,      \
        RV_INT              n,      \
        RV_INT              k,      \
        GEMM_TYPE           alpha,  \
        const GEMM_TYPE*    a,      \
        RV_INT              lda,    \
        const GEMM_TYPE*    b,      \
        RV_INT              ldb,    \
        GEMM_TYPE           beta,   \
        GEMM_TYPE*          c,      \
        RV_INT              ldc

#define CBLAS_GEMM_ARG_NAMES    \
        layout,                 \
        transa,                 \
        transb,                 \
        m,                      \
        n,                      \
        k,                      \
        alpha,                  \
        a,                      \
        lda,                    \
        b,                      \
        ldb,                    \
        beta,                   \
        c,                      \
        ldc

/* CBLAS-Compatible Library Function such as cblas_sgemm or cblas_dgemm */
void GEMM_FUNC(CBLAS_GEMM_ARGS);

/* Verify that computed matrix multiply is correct based on golden model
 * Returns 0 iff mat_c's values are within specified tolerances of their
 * corresponding values in mat_c_correct. */
int check_gemm_tolerance(
        CBLAS_GEMM_ARGS,
        GEMM_TYPE* correct,
        GEMM_TYPE* tolerance);

/* Computes the "correct" and tolerance matrices for S- and DGEMM
 * If rwarm>0, the (possibly non-idempotent) GEMM will be applied repeatedly. */
int correct_gemm(
        CBLAS_GEMM_ARGS,
        GEMM_TYPE* correct,
        GEMM_TYPE* tolerance
        );


#ifdef DEBUG
#define debugf(...) fprintf(stderr, __VA_ARGS__)
#else
#define debugf(...)
#endif // DEBUG

#include <inttypes.h>
#include <stddef.h>

#if __riscv_xlen == 32
#define _RD_COUNTER(COUNTER) \
  static uint64_t rd ## COUNTER() \
  { \
    uint32_t lo; \
    uint32_t hi0, hi1; \
    do { \
      asm volatile ("rd" #COUNTER "h %0" : "=r" (hi0)); \
      asm volatile ("rd" #COUNTER " %0" : "=r" (lo)); \
      asm volatile ("rd" #COUNTER "h %0" : "=r" (hi1)); \
    } while (hi0 != hi1); \
    return (uint64_t) lo + ((uint64_t) hi1 << 32); \
  }
#elif __riscv_xlen == 64
#define _RD_COUNTER(COUNTER) \
    static uint64_t rd ## COUNTER() \
    { \
        uint64_t res; \
        asm volatile ("rd" #COUNTER " %0" : "=r" (res)); \
        return res; \
    }
#endif

#ifdef __riscv
_RD_COUNTER(cycle);   // rdcycle()
_RD_COUNTER(time);    // rdtime()
_RD_COUNTER(instret); // rdinstret()

#endif // __riscv

#endif // GEMM_H
