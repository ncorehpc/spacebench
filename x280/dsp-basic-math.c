/* Copyright 2019 SiFive, Inc */
/* SPDX-License-Identifier: Apache-2.0 */

// This example shows how to implement the SAXPY[1] kernel using sifive-dsp library.
// VecY = ScalarA * VecX + VecY
//
// [1] https://en.wikipedia.org/wiki/Basic_Linear_Algebra_Subprograms#Level_1

#include <math.h>
#include <riscv_vector.h>
#include <riscv_math.h>

#define MAX_BLOCKSIZE 4096

float_t dsp_temp_f32[MAX_BLOCKSIZE];

// The rvv-intrinsic implementation.
void saxpy_rvv_intrinsic(float_t *x, float_t a, float_t *y, uint32_t length)
{
  uint32_t vl = 0;

  vfloat32m8_t vVecX, vVecY;

  for (; (vl = vsetvl_e32m8(length)) > 0; length -= vl) {
    vVecX = vle32_v_f32m8(x, vl);
    x += vl;
    vVecY = vle32_v_f32m8(y, vl);
    vVecY = vfmacc_vf_f32m8(vVecY, a, vVecX, vl);
    vse32_v_f32m8(y, vVecY, vl);
    y += vl;
  }
}

void cblas_saxpy(const int N, const float alpha, const float_t *X, const int incX, float_t *Y, const int incY)
{
 saxpy_rvv_intrinsic(X,alpha,Y,N);
}


// The implementation using cmsis-dsp[1] compatible interface.
//
// [1] https://github.com/ARM-software/CMSIS_5
void saxpy_dsp(float_t *x, float_t a, float_t *y, uint32_t length)
{
  riscv_scale_f32(x, a, dsp_temp_f32, length);
  riscv_add_f32(dsp_temp_f32, y, y, length);
}

