#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>
#include <sys/time.h>

#if defined(__ARM_ARCH_ISA_A64) || defined(__aarch64__) || defined(_M_ARM64) || defined(__riscv)
#include <cblas.h>
#include <arm_neon.h>
#include <omp.h>
#endif

//set data types
#define INT8_TYPE int8_t
#define INT16_TYPE int16_t
#define INT32_TYPE int32_t
#define INT64_TYPE int64_t

#define SPFP_TYPE float32_t
#define DPFP_TYPE float64_t

//set NEON vector sizes
#define INT8_vectorSize 16
#define INT16_vectorSize 8
#define INT32_vectorSize 4
#define INT64_vectorSize 2
#define SPFP_vectorSize 4
#define DPFP_vectorSize 2

//set NEON vector data types
#define INT8_vectorType int8x16_t
#define INT16_vectorType int16x8_t
#define INT32_vectorType int32x4_t
#define INT64_vectorType int64x2_t
#define SPFP_vectorType float32x4_t
#define DPFP_vectorType float64x2_t

//setup timer with microsecond resolution
#define TIME_STRUCT struct timeval
#define TIME_GET(time) gettimeofday((time),NULL)
#define TIME_DOUBLE(time) ((time).tv_sec+1E-6*(time).tv_usec)
#define TIME_RUNTIME(start,end) TIME_DOUBLE(end)-TIME_DOUBLE(start)

//set random seed
#define SEED 376134299

//benchmark functions
double matrix_mult_int8(int size, int threads);
double matrix_mult_int16(int size, int threads);
double matrix_mult_int32(int size, int threads);
double matrix_mult_spfp(int size, int threads);
double matrix_mult_dpfp(int size, int threads);
double matrix_add_int8(int size, int threads);
double matrix_add_int16(int size, int threads);
double matrix_add_int32(int size, int threads);
double matrix_add_spfp(int size, int threads);
double matrix_add_dpfp(int size, int threads);
double matrix_conv_int8(int size, int threads);
double matrix_conv_int16(int size, int threads);
double matrix_conv_int32(int size, int threads);
double matrix_conv_spfp(int size, int threads);
double matrix_conv_dpfp(int size, int threads);
double matrix_trans_int8(int size, int threads);
double matrix_trans_int16(int size, int threads);
double matrix_trans_int32(int size, int threads);
double matrix_trans_spfp(int size, int threads);
double matrix_trans_dpfp(int size, int threads);
double keplers_eq_int8(int size, int threads);
double keplers_eq_int16(int size, int threads);
double keplers_eq_int32(int size, int threads);
double keplers_eq_spfp(int size, int threads);
double keplers_eq_dpfp(int size, int threads);
double cw_eq_spfp(int size, int threads);
double cw_eq_dpfp(int size, int threads);
double matrix_mult_blas_spfp(int size, int threads);
double matrix_mult_blas_dpfp(int size, int threads);
double matrix_add_blas_spfp(int size, int threads);
double matrix_add_blas_dpfp(int size, int threads);
double matrix_conv_blas_spfp(int size, int threads);
double matrix_conv_blas_dpfp(int size, int threads);
