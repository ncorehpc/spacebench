# LICENSE #

Copyright (c) 2017 NSF Center for High-Performance Reconfigurable
Computing (CHREC), University of Pittsburgh. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of CHREC.

### What is this repository for? ###

Benchmarking code for the most common Space algorithms including:

"matrix_mult", "matrix_add", "matrix_conv", "matrix_trans", "keplers_eq", "cw_eq", "matrix_mult_blas", "matrix_add_blas", "matrix_conv_blas"

### Version

HPSC Version of spacebench

### Information on contents

* spacebench.c - C source for main routine
* spacebench.h - C source headers
* benchmarks.c - C source for benchmark routines
* Makefile - make file used for compilation
* inputs.txt - command line input specifications
* license.txt - license file
* run.sh - shell script for automation
* run_perf.sh - shell script for collecting performance data

### Dependencies

OpenBLAS from:

https://github.com/xianyi/OpenBLAS.git

or blas/atlas installed via your OS package manager.

gcc or clang cross compiler or native compiler

gnu make

**note: cross and native works on aarch64. cross bare-metal only works on riscv64.**

### Building the Benchmark ###

To Cross Compile on x86:

`make TARGET=<target>`

where TARGET is one of:

aarch64
x280

or compile directly on the target using

`make`

**note: native compile currently only supported on ARM**

### Running the Benchmarks

 `run.sh` executes all the benchmarks and prints the results in CSV format.

### Performance Measurements

`perf_run.sh`

Executes perf (5 runs each benchmark) and collects performance counters.  It's possible to change the counters by editing the `perfstats.<arch>` file where `<arch>` is `aarch64`.

### Command line inputs to spacebench

`./spacebench benchmark# datatype# size #threads #iterations`

`#Benchmark` 

1	matrix multiplication

2	matrix addition

3	matrix convolution

4	matrix transpose

5	Kepler's equation

6	Clohessy-Wiltshire equations

7	matrix multiplication with BLAS

8	matrix addition with BLAS

9	matrix convolution with BLAS

`#Datatype` 

1 - Int8

2 - Int16

3 - Int32

4 - SPFP

5 - DPFP

size is problem size n

`#threads` specifies number of OpenMP threads

`#iterations` is how many iterations to compute

Note:

Clohessy-Wiltshire equations only support SPFP and DPFP

BLAS routines only support SPFP and DPFP

### Who do I talk to? ###

###### HPSC version maintainer

Thea-Martine Gauthier - nCore HPC LLC (thea@ncorehpc.com)
