#include <spacebench.h>

int main(int argc, char *argv[])
{
    //parameters taken as command line arguments
    int benchmark = atoi(argv[1]);
    int data_type = atoi(argv[2]);
    int size = atoi(argv[3]);
    int threads = atoi(argv[4]);
    int iterations = atoi(argv[5]);
    //initialize variables
    int i;
    double runtime = 0;
    //initialize strings used for printout
    const char *benchmarks[9] = {"matrix_mult", "matrix_add", "matrix_conv", "matrix_trans", "keplers_eq", "cw_eq", "matrix_mult_blas", "matrix_add_blas", "matrix_conv_blas"};
    const char *data_types[5] = {"int8", "int16", "int32", "spfp", "dpfp"};

    //compute iterations of specified benchmark
    for (i = 0; i < iterations; i++)
    {
        //matrix multiplication
        if (benchmark == 1)
        {
            switch (data_type)
            {
                case (1):
                    runtime += matrix_mult_int8(size, threads);
                    break;

                case (2):
                    runtime += matrix_mult_int16(size, threads);
                    break;

                case (3):
                    runtime += matrix_mult_int32(size, threads);
                    break;

                case (4):
                    runtime += matrix_mult_spfp(size, threads);
                    break;

                case (5):
                    runtime += matrix_mult_dpfp(size, threads);
                    break;
            }
        }

        //matrix addition
        if (benchmark == 2)
        {
            switch (data_type)
            {
                case (1):
                    runtime += matrix_add_int8(size, threads);
                    break;

                case (2):
                    runtime += matrix_add_int16(size, threads);
                    break;

                case (3):
                    runtime += matrix_add_int32(size, threads);
                    break;

                case (4):
                    runtime += matrix_add_spfp(size, threads);
                    break;

                case (5):
                    runtime += matrix_add_dpfp(size, threads);
                    break;
            }
        }

        //matrix convolution
        if (benchmark == 3)
        {
            switch (data_type)
            {
                case (1):
                    runtime += matrix_conv_int8(size, threads);
                    break;

                case (2):
                    runtime += matrix_conv_int16(size, threads);
                    break;

                case (3):
                    runtime += matrix_conv_int32(size, threads);
                    break;

                case (4):
                    runtime += matrix_conv_spfp(size, threads);
                    break;

                case (5):
                    runtime += matrix_conv_dpfp(size, threads);
                    break;
            }
        }

        //matrix transpose
        if (benchmark == 4)
        {
            switch (data_type)
            {
                case (1):
                    runtime += matrix_trans_int8(size, threads);
                    break;

                case (2):
                    runtime += matrix_trans_int16(size, threads);
                    break;

                case (3):
                    runtime += matrix_trans_int32(size, threads);
                    break;

                case (4):
                    runtime += matrix_trans_spfp(size, threads);
                    break;

                case (5):
                    runtime += matrix_trans_dpfp(size, threads);
                    break;
            }
        }
        //Kepler's equation
        else if (benchmark == 5)
        {
            switch (data_type)
            {
                case (1):
                    runtime += keplers_eq_int8(size, threads);
                    break;

                case (2):
                    runtime += keplers_eq_int16(size, threads);
                    break;

                case (3):
                    runtime += keplers_eq_int32(size, threads);
                    break;

                case (4):
                    runtime += keplers_eq_spfp(size, threads);
                    break;

                case (5):
                    runtime += keplers_eq_dpfp(size, threads);
                    break;
            }
        }
        //Clohessy-Wiltshire equations
        else if (benchmark == 6)
        {
            switch (data_type)
            {
                case (4):
                    runtime += cw_eq_spfp(size, threads);
                    break;

                case (5):
                    runtime += cw_eq_dpfp(size, threads);
                    break;
            }
        }

        //matrix multiplication with BLAS
        if (benchmark == 7)
        {
            switch (data_type)
            {
                case (4):
                    runtime += matrix_mult_blas_spfp(size, threads);
                    break;

                case (5):
                    runtime += matrix_mult_blas_dpfp(size, threads);
                    break;
            }
        }

        //matrix addition with BLAS
        if (benchmark == 8)
        {
            switch (data_type)
            {
                case (4):
                    runtime += matrix_add_blas_spfp(size, threads);
                    break;

                case (5):
                    runtime += matrix_add_blas_dpfp(size, threads);
                    break;
            }
        }

        //matrix convolution with BLAS
        if (benchmark == 9)
        {
            switch (data_type)
            {
                case (4):
                    runtime += matrix_conv_blas_spfp(size, threads);
                    break;

                case (5):
                    runtime += matrix_conv_blas_dpfp(size, threads);
                    break;
            }
        }
    }

    //print results
    printf("%s,%s,%d,%d,%f\n", benchmarks[benchmark - 1], data_types[data_type - 1], size, threads, runtime / iterations);
    printf("runtime per iteration:%f\n", runtime / iterations);
    return (0);
}
