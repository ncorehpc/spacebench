#!/bin/bash

perfstats=perfstats.aarch64

#matrix multiplication
for i in 1024
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./$perfstats 1 $j $i $k 10
		done
	done
done

#matrix addition
for i in 2048
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./$perfstats 2 $j $i $k 100
		done
	done
done

#matrix convolution
for i in 2048
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./$perfstats 3 $j $i $k 100
		done
	done
done

#matrix transpose
for i in 2048
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./$perfstats 4 $j $i $k 100
		done
	done
done

#Kepler's equation
for i in 1024
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./$perfstats 5 $j $i $k 1000
		done
	done
done

#Clohessy-Wiltshire equations
for i in 2048
do
	for j in 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./$perfstats 6 $j $i $k 1000
		done
	done
done

#matrix multiplication with BLAS
for i in 1024
do
	for j in 4 5
	do
		for k in 1
		do
			./$perfstats 7 $j $i $k 10
		done
	done
done

#matrix addition with BLAS
for i in 2048
do
	for j in 4 5
	do
		for k in 1
		do
			./$perfstats 8 $j $i $k 100
		done
	done
done

#matrix convolution with BLAS
for i in 2048
do
	for j in 4 5
	do
		for k in 1
		do
			./$perfstats 9 $j $i $k 100
		done
	done
done
