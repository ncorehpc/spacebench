#!/bin/bash

#matrix multiplication
for i in 1024
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./spacebench 1 $j $i $k 10
		done
	done
done

#matrix addition
for i in 2048
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./spacebench 2 $j $i $k 100
		done
	done
done

#matrix convolution
for i in 2048
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./spacebench 3 $j $i $k 100
		done
	done
done

#matrix transpose
for i in 2048
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./spacebench 4 $j $i $k 100
		done
	done
done

#Kepler's equation
for i in 1024
do
	for j in 1 2 3 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./spacebench 5 $j $i $k 1000
		done
	done
done

#Clohessy-Wiltshire equations
for i in 2048
do
	for j in 4 5
	do
		for k in 1 2 4 8 16 32
		do
			./spacebench 6 $j $i $k 1000
		done
	done
done

#matrix multiplication with BLAS
for i in 1024
do
	for j in 4 5
	do
		for k in 1
		do
			./spacebench 7 $j $i $k 10
		done
	done
done

#matrix addition with BLAS
for i in 2048
do
	for j in 4 5
	do
		for k in 1
		do
			./spacebench 8 $j $i $k 100
		done
	done
done

#matrix convolution with BLAS
for i in 2048
do
	for j in 4 5
	do
		for k in 1
		do
			./spacebench 9 $j $i $k 100
		done
	done
done