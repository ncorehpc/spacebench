ARCH=$(shell echo `uname -m`)

ifeq ($(ARCH),aarch64)
include make.aarch64
 CC=gcc
endif

ifeq ($(ARCH),riscv64)
include make.riscv64
endif

ifeq ($(ARCH),x86_64)
include make.cross
endif

all:  spacebench build_vars

OBJECTS+=spacebench.c benchmarks.c
spacebench: build_openblas
	$(CC) $(OPT) $(FLAGS) $(DEFINES) $(OBJECTS) -o $@ $(LFLAGS)

build_vars:
	@echo "COMPILER:$(CC)" > build_vars.txt
	@echo "CFLAGS:$(OPT) $(FLAGS)" >> build_vars.txt
	@echo "LFLAGS:$(LFLAGS)" >> build_vars.txt

build_openblas:
	make -C $(TOPDIR)/hpsc-openblas HOSTCC=gcc TARGET=$(TARGET) NUM_THREADS=8 CC=riscv64-unknown-linux-gnu-clang FC=riscv64-unknown-linux-gnu-gfortran
	make -C $(TOPDIR)/hpsc-openblas HOSTCC=gcc TARGET=$(TARGET) NUM_THREADS=8 CC=riscv64-unknown-linux-gnu-clang FC=riscv64-unknown-linux-gnu-gfortran PREFIX=build install

clean:
	rm -f spacebench build_vars.txt
