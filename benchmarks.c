#include "./spacebench.h"

double matrix_mult_int8(int size, int threads)
{
    // Note: this code assumes that size is a positive multiple of four
    assert(size > 0 && (size % 4 == 0));
    size_t N = size;
    
    int8_t*  A = aligned_alloc(4096, N * N * sizeof(int8_t));
    int8_t*  B = aligned_alloc(4096, N * N * sizeof(int8_t));
    int16_t* C = aligned_alloc(4096, N * N * sizeof(int16_t));

    // randomize A and B
    srand(SEED);
    for (size_t i = 0; i < N; ++i)
        for (size_t j = 0; j < N; ++j)
        {
#ifdef FAST_INIT // speedup for emulation
            A[i * N + j] = i;
            B[i * N + j] = i;
#else
            A[i * N + j] = rand();
            B[i * N + j] = rand();
#endif
        }
 
    TIME_STRUCT start, end;
    TIME_GET(&start);

#pragma omp parallel for num_threads(threads) firstprivate(A, B, C, N)
    for (size_t i = 0; i < N; i += 4) {
        size_t ioff0 = (i + 0)*N;
        size_t ioff1 = (i + 1)*N;
        size_t ioff2 = (i + 2)*N;
        size_t ioff3 = (i + 3)*N;
        for (size_t j = 0, avl = N, vl; avl; avl -= vl, j += vl) {
            vl = vsetvl_e8m4 (avl);
            size_t k = 0;
            vint8m2_t Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            vint16m4_t Cvec0 = vwmul_vx_i16m4 (Bvec, A[ioff0 + k], vl);
            vint16m4_t Cvec1 = vwmul_vx_i16m4 (Bvec, A[ioff1 + k], vl);
            vint16m4_t Cvec2 = vwmul_vx_i16m4 (Bvec, A[ioff2 + k], vl);
            vint16m4_t Cvec3 = vwmul_vx_i16m4 (Bvec, A[ioff3 + k], vl);
            for (k = 1; k < N - 1; ++k) {
                Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
                Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
                Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
                Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
                Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            }
            Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
            vse16_v_i16m4 (&C[ioff0 + j], Cvec0, vl);
            Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
            vse16_v_i16m4 (&C[ioff1 + j], Cvec1, vl);
            Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
            vse16_v_i16m4 (&C[ioff2 + j], Cvec2, vl);
            Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            vse16_v_i16m4 (&C[ioff3 + j], Cvec3, vl);
        }
    }

    TIME_GET(&end);
    
    free(C);
    free(B);
    free(A);

    return TIME_RUNTIME(start, end);
}

double matrix_mult_int16(int size, int threads)
{
    // Note: this code assumes that size is a positive multiple of four
    assert(size > 0 && (size % 4 == 0));
    size_t N = size;
    
    int16_t*  A = aligned_alloc(4096, N * N * sizeof(int16_t));		//int8_t*  A = aligned_alloc(4096, N * N * sizeof(int8_t));
    int16_t*  B = aligned_alloc(4096, N * N * sizeof(int16_t));		//int8_t*  B = aligned_alloc(4096, N * N * sizeof(int8_t));
    int32_t* C = aligned_alloc(4096, N * N * sizeof(int32_t));		//int16_t* C = aligned_alloc(4096, N * N * sizeof(int16_t));


    // randomize A and B
    srand(SEED);
    for (size_t i = 0; i < N; ++i)
        for (size_t j = 0; j < N; ++j)
        {
#ifdef FAST_INIT // speedup for emulation
            A[i * N + j] = i;
            B[i * N + j] = i;
#else
            A[i * N + j] = rand();
            B[i * N + j] = rand();
#endif
        }
 
    TIME_STRUCT start, end;
    TIME_GET(&start);

#pragma omp parallel for num_threads(threads) firstprivate(A, B, C, N)
    for (size_t i = 0; i < N; i += 4) {
        size_t ioff0 = (i + 0)*N;
        size_t ioff1 = (i + 1)*N;
        size_t ioff2 = (i + 2)*N;
        size_t ioff3 = (i + 3)*N;
        for (size_t j = 0, avl = N, vl; avl; avl -= vl, j += vl) {
            vl = vsetvl_e16m4 (avl);											//vl = vsetvl_e8m4 (avl);
            size_t k = 0;
            vint16m2_t Bvec = vle16_v_i16m2 (&B[k * N + j], vl); 				//vint8m2_t Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            vint32m4_t Cvec0 = vwmul_vx_i32m4 (Bvec, A[ioff0 + k], vl);			//vint16m4_t Cvec0 = vwmul_vx_i16m4 (Bvec, A[ioff0 + k], vl);
            vint32m4_t Cvec1 = vwmul_vx_i32m4 (Bvec, A[ioff1 + k], vl); 		//vint16m4_t Cvec1 = vwmul_vx_i16m4 (Bvec, A[ioff1 + k], vl);
            vint32m4_t Cvec2 = vwmul_vx_i32m4 (Bvec, A[ioff2 + k], vl); 		//vint16m4_t Cvec2 = vwmul_vx_i16m4 (Bvec, A[ioff2 + k], vl);
            vint32m4_t Cvec3 = vwmul_vx_i32m4 (Bvec, A[ioff3 + k], vl); 		//vint16m4_t Cvec3 = vwmul_vx_i16m4 (Bvec, A[ioff3 + k], vl);
            for (k = 1; k < N - 1; ++k) {
                Bvec = vle16_v_i16m2 (&B[k * N + j], vl); 						//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
                Cvec0 = vwmacc_vx_i32m4 (Cvec0, A[ioff0 + k], Bvec, vl); 		//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
                Cvec1 = vwmacc_vx_i32m4 (Cvec1, A[ioff1 + k], Bvec, vl);		//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
                Cvec2 = vwmacc_vx_i32m4 (Cvec2, A[ioff2 + k], Bvec, vl);		//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
                Cvec3 = vwmacc_vx_i32m4 (Cvec3, A[ioff3 + k], Bvec, vl);		//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            }
            Bvec = vle16_v_i16m2 (&B[k * N + j], vl);							//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            Cvec0 = vwmacc_vx_i32m4 (Cvec0, A[ioff0 + k], Bvec, vl);			//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
            vse32_v_i32m4 (&C[ioff0 + j], Cvec0, vl);							//vse16_v_i16m4 (&C[ioff0 + j], Cvec0, vl);
            Cvec1 = vwmacc_vx_i32m4 (Cvec1, A[ioff1 + k], Bvec, vl);			//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
            vse32_v_i32m4 (&C[ioff1 + j], Cvec1, vl);							//vse16_v_i16m4 (&C[ioff1 + j], Cvec1, vl);
            Cvec2 = vwmacc_vx_i32m4 (Cvec2, A[ioff2 + k], Bvec, vl);			//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
            vse32_v_i32m4 (&C[ioff2 + j], Cvec2, vl);							//vse16_v_i16m4 (&C[ioff2 + j], Cvec2, vl);
            Cvec3 = vwmacc_vx_i32m4 (Cvec3, A[ioff3 + k], Bvec, vl);			//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            vse32_v_i32m4 (&C[ioff3 + j], Cvec3, vl);							//vse16_v_i16m4 (&C[ioff3 + j], Cvec3, vl);
        }
    }

    TIME_GET(&end);
    
    free(C);
    free(B);
    free(A);

    return TIME_RUNTIME(start, end);
}

double matrix_mult_int32(int size, int threads)
{
    // Note: this code assumes that size is a positive multiple of four
    assert(size > 0 && (size % 4 == 0));
    size_t N = size;
    
    int32_t*  A = aligned_alloc(4096, N * N * sizeof(int32_t));		//int8_t*  A = aligned_alloc(4096, N * N * sizeof(int8_t));
    int32_t*  B = aligned_alloc(4096, N * N * sizeof(int32_t));		//int8_t*  B = aligned_alloc(4096, N * N * sizeof(int8_t));
    int64_t* C = aligned_alloc(4096, N * N * sizeof(int64_t));		//int16_t* C = aligned_alloc(4096, N * N * sizeof(int16_t));


    // randomize A and B
    srand(SEED);
    for (size_t i = 0; i < N; ++i)
        for (size_t j = 0; j < N; ++j)
        {
#ifdef FAST_INIT // speedup for emulation
            A[i * N + j] = i;
            B[i * N + j] = i;
#else
            A[i * N + j] = rand();
            B[i * N + j] = rand();
#endif
        }
 
    TIME_STRUCT start, end;
    TIME_GET(&start);

#pragma omp parallel for num_threads(threads) firstprivate(A, B, C, N)
    for (size_t i = 0; i < N; i += 4) {
        size_t ioff0 = (i + 0)*N;
        size_t ioff1 = (i + 1)*N;
        size_t ioff2 = (i + 2)*N;
        size_t ioff3 = (i + 3)*N;
        for (size_t j = 0, avl = N, vl; avl; avl -= vl, j += vl) {
            vl = vsetvl_e32m4 (avl);											//vl = vsetvl_e8m4 (avl);
            size_t k = 0;
            vint32m2_t Bvec = vle32_v_i32m2 (&B[k * N + j], vl); 				//vint8m2_t Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            vint64m4_t Cvec0 = vwmul_vx_i64m4 (Bvec, A[ioff0 + k], vl);			//vint16m4_t Cvec0 = vwmul_vx_i16m4 (Bvec, A[ioff0 + k], vl);
            vint64m4_t Cvec1 = vwmul_vx_i64m4 (Bvec, A[ioff1 + k], vl); 		//vint16m4_t Cvec1 = vwmul_vx_i16m4 (Bvec, A[ioff1 + k], vl);
            vint64m4_t Cvec2 = vwmul_vx_i64m4 (Bvec, A[ioff2 + k], vl); 		//vint16m4_t Cvec2 = vwmul_vx_i16m4 (Bvec, A[ioff2 + k], vl);
            vint64m4_t Cvec3 = vwmul_vx_i64m4 (Bvec, A[ioff3 + k], vl); 		//vint16m4_t Cvec3 = vwmul_vx_i16m4 (Bvec, A[ioff3 + k], vl);
            for (k = 1; k < N - 1; ++k) {
                Bvec = vle32_v_i32m2 (&B[k * N + j], vl); 						//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
                Cvec0 = vwmacc_vx_i64m4 (Cvec0, A[ioff0 + k], Bvec, vl); 		//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
                Cvec1 = vwmacc_vx_i64m4 (Cvec1, A[ioff1 + k], Bvec, vl);		//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
                Cvec2 = vwmacc_vx_i64m4 (Cvec2, A[ioff2 + k], Bvec, vl);		//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
                Cvec3 = vwmacc_vx_i64m4 (Cvec3, A[ioff3 + k], Bvec, vl);		//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            }
            Bvec = vle32_v_i32m2 (&B[k * N + j], vl);							//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            Cvec0 = vwmacc_vx_i64m4 (Cvec0, A[ioff0 + k], Bvec, vl);			//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
            vse64_v_i64m4 (&C[ioff0 + j], Cvec0, vl);							//vse16_v_i16m4 (&C[ioff0 + j], Cvec0, vl);
            Cvec1 = vwmacc_vx_i64m4 (Cvec1, A[ioff1 + k], Bvec, vl);			//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
            vse64_v_i64m4 (&C[ioff1 + j], Cvec1, vl);							//vse16_v_i16m4 (&C[ioff1 + j], Cvec1, vl);
            Cvec2 = vwmacc_vx_i64m4 (Cvec2, A[ioff2 + k], Bvec, vl);			//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
            vse64_v_i64m4 (&C[ioff2 + j], Cvec2, vl);							//vse16_v_i16m4 (&C[ioff2 + j], Cvec2, vl);
            Cvec3 = vwmacc_vx_i64m4 (Cvec3, A[ioff3 + k], Bvec, vl);			//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            vse64_v_i64m4 (&C[ioff3 + j], Cvec3, vl);							//vse16_v_i16m4 (&C[ioff3 + j], Cvec3, vl);
        }
    }

    TIME_GET(&end);
    
    free(C);
    free(B);
    free(A);

    return TIME_RUNTIME(start, end);
}

double matrix_mult_spfp(int size, int threads)
{
    // Note: this code assumes that size is a positive multiple of four
    assert(size > 0 && (size % 4 == 0));
    size_t N = size;
    
    float32_t*  A = aligned_alloc(4096, N * N * sizeof(float32_t));		//int8_t*  A = aligned_alloc(4096, N * N * sizeof(int8_t));
    float32_t*  B = aligned_alloc(4096, N * N * sizeof(float32_t));		//int8_t*  B = aligned_alloc(4096, N * N * sizeof(int8_t));
    float32_t* C = aligned_alloc(4096, N * N * sizeof(float32_t));		//int16_t* C = aligned_alloc(4096, N * N * sizeof(int16_t));


    // randomize A and B
    srand(SEED);
    for (size_t i = 0; i < N; ++i)
        for (size_t j = 0; j < N; ++j)
        {
#ifdef FAST_INIT // speedup for emulation
            A[i * N + j] = i;
            B[i * N + j] = i;
#else
            A[i * N + j] = rand();
            B[i * N + j] = rand();
#endif
        }
 
    TIME_STRUCT start, end;
    TIME_GET(&start);

#pragma omp parallel for num_threads(threads) firstprivate(A, B, C, N)
    for (size_t i = 0; i < N; i += 4) {
        size_t ioff0 = (i + 0)*N;
        size_t ioff1 = (i + 1)*N;
        size_t ioff2 = (i + 2)*N;
        size_t ioff3 = (i + 3)*N;
        for (size_t j = 0, avl = N, vl; avl; avl -= vl, j += vl) {
            vl = vsetvl_e32m4 (avl);											//vl = vsetvl_e8m4 (avl);
            size_t k = 0;
            vfloat32m4_t Bvec = vle32_v_f32m4 (&B[k * N + j], vl); 				//vint8m2_t Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            vfloat32m4_t Cvec0 = vfmul_vf_f32m4 (Bvec, A[ioff0 + k], vl);		//vint16m4_t Cvec0 = vwmul_vx_i16m4 (Bvec, A[ioff0 + k], vl);
            vfloat32m4_t Cvec1 = vfmul_vf_f32m4 (Bvec, A[ioff1 + k], vl); 		//vint16m4_t Cvec1 = vwmul_vx_i16m4 (Bvec, A[ioff1 + k], vl);
            vfloat32m4_t Cvec2 = vfmul_vf_f32m4 (Bvec, A[ioff2 + k], vl); 		//vint16m4_t Cvec2 = vwmul_vx_i16m4 (Bvec, A[ioff2 + k], vl);
            vfloat32m4_t Cvec3 = vfmul_vf_f32m4 (Bvec, A[ioff3 + k], vl); 		//vint16m4_t Cvec3 = vwmul_vx_i16m4 (Bvec, A[ioff3 + k], vl);
            for (k = 1; k < N - 1; ++k) {
                Bvec = vle32_v_f32m4 (&B[k * N + j], vl); 						//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
                Cvec0 = vfmacc_vf_f32m4 (Cvec0, A[ioff0 + k], Bvec, vl); 		//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
                Cvec1 = vfmacc_vf_f32m4 (Cvec1, A[ioff1 + k], Bvec, vl);		//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
                Cvec2 = vfmacc_vf_f32m4 (Cvec2, A[ioff2 + k], Bvec, vl);		//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
                Cvec3 = vfmacc_vf_f32m4 (Cvec3, A[ioff3 + k], Bvec, vl);		//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            }
            Bvec = vle32_v_f32m4 (&B[k * N + j], vl);							//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            Cvec0 = vfmacc_vf_f32m4 (Cvec0, A[ioff0 + k], Bvec, vl);			//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
            vse32_v_f32m4 (&C[ioff0 + j], Cvec0, vl);							//vse16_v_i16m4 (&C[ioff0 + j], Cvec0, vl);
            Cvec1 = vfmacc_vf_f32m4 (Cvec1, A[ioff1 + k], Bvec, vl);			//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
            vse32_v_f32m4 (&C[ioff1 + j], Cvec1, vl);							//vse16_v_i16m4 (&C[ioff1 + j], Cvec1, vl);
            Cvec2 = vfmacc_vf_f32m4 (Cvec2, A[ioff2 + k], Bvec, vl);			//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
            vse32_v_f32m4 (&C[ioff2 + j], Cvec2, vl);							//vse16_v_i16m4 (&C[ioff2 + j], Cvec2, vl);
            Cvec3 = vfmacc_vf_f32m4 (Cvec3, A[ioff3 + k], Bvec, vl);			//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            vse32_v_f32m4 (&C[ioff3 + j], Cvec3, vl);							//vse16_v_i16m4 (&C[ioff3 + j], Cvec3, vl);
        }
    }

    TIME_GET(&end);
    
    free(C);
    free(B);
    free(A);

    return TIME_RUNTIME(start, end);
}

double matrix_mult_dpfp(int size, int threads)
{
    // Note: this code assumes that size is a positive multiple of four
    assert(size > 0 && (size % 4 == 0));
    size_t N = size;
    
    float64_t*  A = aligned_alloc(4096, N * N * sizeof(float64_t));		//int8_t*  A = aligned_alloc(4096, N * N * sizeof(int8_t));
    float64_t*  B = aligned_alloc(4096, N * N * sizeof(float64_t));		//int8_t*  B = aligned_alloc(4096, N * N * sizeof(int8_t));
    float64_t* C = aligned_alloc(4096, N * N * sizeof(float64_t));		//int16_t* C = aligned_alloc(4096, N * N * sizeof(int16_t));


    // randomize A and B
    srand(SEED);
    for (size_t i = 0; i < N; ++i)
        for (size_t j = 0; j < N; ++j)
        {
#ifdef FAST_INIT // speedup for emulation
            A[i * N + j] = i;
            B[i * N + j] = i;
#else
            A[i * N + j] = rand();
            B[i * N + j] = rand();
#endif
        }
 
    TIME_STRUCT start, end;
    TIME_GET(&start);

#pragma omp parallel for num_threads(threads) firstprivate(A, B, C, N)
    for (size_t i = 0; i < N; i += 4) {
        size_t ioff0 = (i + 0)*N;
        size_t ioff1 = (i + 1)*N;
        size_t ioff2 = (i + 2)*N;
        size_t ioff3 = (i + 3)*N;
        for (size_t j = 0, avl = N, vl; avl; avl -= vl, j += vl) {
            vl = vsetvl_e64m4 (avl);											//vl = vsetvl_e8m4 (avl);
            size_t k = 0;
            vfloat64m4_t Bvec = vle64_v_f64m4 (&B[k * N + j], vl); 				//vint8m2_t Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            vfloat64m4_t Cvec0 = vfmul_vf_f64m4 (Bvec, A[ioff0 + k], vl);		//vint16m4_t Cvec0 = vwmul_vx_i16m4 (Bvec, A[ioff0 + k], vl);
            vfloat64m4_t Cvec1 = vfmul_vf_f64m4 (Bvec, A[ioff1 + k], vl); 		//vint16m4_t Cvec1 = vwmul_vx_i16m4 (Bvec, A[ioff1 + k], vl);
            vfloat64m4_t Cvec2 = vfmul_vf_f64m4 (Bvec, A[ioff2 + k], vl); 		//vint16m4_t Cvec2 = vwmul_vx_i16m4 (Bvec, A[ioff2 + k], vl);
            vfloat64m4_t Cvec3 = vfmul_vf_f64m4 (Bvec, A[ioff3 + k], vl); 		//vint16m4_t Cvec3 = vwmul_vx_i16m4 (Bvec, A[ioff3 + k], vl);
            for (k = 1; k < N - 1; ++k) {
                Bvec = vle64_v_f64m4 (&B[k * N + j], vl); 						//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
                Cvec0 = vfmacc_vf_f64m4 (Cvec0, A[ioff0 + k], Bvec, vl); 		//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
                Cvec1 = vfmacc_vf_f64m4 (Cvec1, A[ioff1 + k], Bvec, vl);		//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
                Cvec2 = vfmacc_vf_f64m4 (Cvec2, A[ioff2 + k], Bvec, vl);		//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
                Cvec3 = vfmacc_vf_f64m4 (Cvec3, A[ioff3 + k], Bvec, vl);		//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            }
            Bvec = vle64_v_f64m4 (&B[k * N + j], vl);							//Bvec = vle8_v_i8m2 (&B[k * N + j], vl);
            Cvec0 = vfmacc_vf_f64m4 (Cvec0, A[ioff0 + k], Bvec, vl);			//Cvec0 = vwmacc_vx_i16m4 (Cvec0, A[ioff0 + k], Bvec, vl);
            vse64_v_f64m4 (&C[ioff0 + j], Cvec0, vl);							//vse16_v_i16m4 (&C[ioff0 + j], Cvec0, vl);
            Cvec1 = vfmacc_vf_f64m4 (Cvec1, A[ioff1 + k], Bvec, vl);			//Cvec1 = vwmacc_vx_i16m4 (Cvec1, A[ioff1 + k], Bvec, vl);
            vse64_v_f64m4 (&C[ioff1 + j], Cvec1, vl);							//vse16_v_i16m4 (&C[ioff1 + j], Cvec1, vl);
            Cvec2 = vfmacc_vf_f64m4 (Cvec2, A[ioff2 + k], Bvec, vl);			//Cvec2 = vwmacc_vx_i16m4 (Cvec2, A[ioff2 + k], Bvec, vl);
            vse64_v_f64m4 (&C[ioff2 + j], Cvec2, vl);							//vse16_v_i16m4 (&C[ioff2 + j], Cvec2, vl);
            Cvec3 = vfmacc_vf_f64m4 (Cvec3, A[ioff3 + k], Bvec, vl);			//Cvec3 = vwmacc_vx_i16m4 (Cvec3, A[ioff3 + k], Bvec, vl);
            vse64_v_f64m4 (&C[ioff3 + j], Cvec3, vl);							//vse16_v_i16m4 (&C[ioff3 + j], Cvec3, vl);
        }
    }

    TIME_GET(&end);
    
    free(C);
    free(B);
    free(A);

    return TIME_RUNTIME(start, end);
}

double matrix_add_int8(int size, int threads)
{
    // initialize random number generator
    int i = 0;
    srand(SEED);
    TIME_STRUCT start, end;

    // allocate memory for matrices
    size_t N = size;
    size_t N2 = size*size;    

    int8_t* A = aligned_alloc(4096, N2 * sizeof(int8_t));
    int8_t* B = aligned_alloc(4096, N2 * sizeof(int8_t));
    int8_t* C = aligned_alloc(4096, N2 * sizeof(int8_t));

    // initialize input matrices to random numbers
    // initialize output matrix to zeros
    for (i = 0; i < N2; i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
        C[i] = 0;
    }

    // start timer
    TIME_GET(&start);

    // serial operation
    if (threads == 1)
    {
        size_t vl;
        for (i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e8m8(N2-i);
            vint8m8_t va = vle8_v_i8m8(A + i, vl);
            vint8m8_t vb = vle8_v_i8m8(B + i, vl);
            vint8m8_t vc = vadd_vv_i8m8(va, vb, vl);
            vse8_v_i8m8(C + i, vc, vl);
        }
    }
    else
    {
        size_t vl = vsetvl_e8m8(N2); // workaround omp loop incr invariance (although it is variant on the final pass)

        #pragma omp parallel for num_threads(threads) firstprivate(vl)
        for (i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e8m8(N2-i);
            vint8m8_t va = vle8_v_i8m8(A + i, vl);
            vint8m8_t vb = vle8_v_i8m8(B + i, vl);
            vint8m8_t vc = vadd_vv_i8m8(va, vb, vl);
            vse8_v_i8m8(C + i, vc, vl);
        }
    }

    // end timer
    TIME_GET(&end);

    // free memory
    free(C);
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_add_int16(int size, int threads)
{
    // initialize random number generator
    int i = 0;
    srand(SEED);
    TIME_STRUCT start, end;

    // allocate memory for matrices
    size_t N = size;
    size_t N2 = size*size;    
    
    int16_t* A = aligned_alloc(4096, N2 * sizeof(int16_t));
    int16_t* B = aligned_alloc(4096, N2 * sizeof(int16_t));
    int16_t* C = aligned_alloc(4096, N2 * sizeof(int16_t));

    // initialize input matrices to random numbers
    // initialize output matrix to zeros
    for (i = 0; i < N2; i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
        C[i] = 0;
    }

    // start timer
    TIME_GET(&start);

    // serial operation
    if (threads == 1)
    {
        size_t vl;
        for (i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e16m8(N2-i);
            vint16m8_t va = vle16_v_i16m8(A + i, vl);
            vint16m8_t vb = vle16_v_i16m8(B + i, vl);
            vint16m8_t vc = vadd_vv_i16m8(va, vb, vl);
            vse16_v_i16m8(C + i, vc, vl);
        }
    }
    else
    {
        size_t vl = vsetvl_e16m8(N2); // workaround omp loop incr invariance (although it is variant on the final pass)

        #pragma omp parallel for num_threads(threads) firstprivate(vl)
        for (i = 0; i < N2; i += vl)
        {
           vl = vsetvl_e16m8(N2-i);
            vint16m8_t va = vle16_v_i16m8(A + i, vl);
            vint16m8_t vb = vle16_v_i16m8(B + i, vl);
            vint16m8_t vc = vadd_vv_i16m8(va, vb, vl);
            vse16_v_i16m8(C + i, vc, vl);
        }
    }

    //end timer
    TIME_GET(&end);

    //free memory
    free(C);
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_add_int32(int size, int threads)
{
    // random number generator
    int i = 0;
    srand(SEED);
    TIME_STRUCT start, end;

    // allocate memory for matrices
    size_t N = size;
    size_t N2 = size*size;    

    int32_t* A = aligned_alloc(4096, N2 * sizeof(int32_t));
    int32_t* B = aligned_alloc(4096, N2 * sizeof(int32_t));
    int32_t* C = aligned_alloc(4096, N2 * sizeof(int32_t));

    // initialize input matrices to random numbers
    // initialize output matrix to zeros
    for (i = 0; i < N2; i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
        C[i] = 0;
    }

    //start timer
    TIME_GET(&start);

    // serial operation
    if (threads == 1)
    {
        size_t vl;
        for (i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e32m8(N2-i);

            vint32m8_t va = vle32_v_i32m8(A + i, vl);
            vint32m8_t vb = vle32_v_i32m8(B + i, vl);
            vint32m8_t vc = vadd_vv_i32m8(va, vb, vl);
            vse32_v_i32m8(C + i, vc, vl);
        }
    }
    else
    {
        size_t vl = vsetvl_e32m8(N2); // workaround omp loop incr invariance (although it is variant on the final pass)

        #pragma omp parallel for num_threads(threads) firstprivate(vl)
        for (i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e32m8(N2-i);
            vint32m8_t va = vle32_v_i32m8(A + i, vl);
            vint32m8_t vb = vle32_v_i32m8(B + i, vl);
            vint32m8_t vc = vadd_vv_i32m8(va, vb, vl);
            vse32_v_i32m8(C + i, vc, vl);
        }
    }

    // end timer
    TIME_GET(&end);

    // free memory
    free(C);
    free(B);
    free(A);
    // compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_add_spfp(int size, int threads)
{
    // random number generator
    srand(SEED);
    TIME_STRUCT start, end;

    // allocate memory for matrices
    size_t N = size;
    size_t N2 = size*size;

    float32_t* A = aligned_alloc(4096, N2 * sizeof(float32_t));
    float32_t* B = aligned_alloc(4096, N2 * sizeof(float32_t));
    float32_t* C = aligned_alloc(4096, N2 * sizeof(float32_t));

    // initialize input matrices to random numbers
    // initialize output matrix to zeros
    for (int i = 0; i < N2; i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
        C[i] = 0;
    }

    // start timer
    TIME_GET(&start);

    // serial operation
    if (threads == 1)
    {
        size_t vl;
        for (int i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e32m8(N2-i);

            vfloat32m8_t va = vle32_v_f32m8(A + i, vl);
            vfloat32m8_t vb = vle32_v_f32m8(B + i, vl);
            vfloat32m8_t vc = vfadd_vv_f32m8(va, vb, vl);
            vse32_v_f32m8(C + i, vc, vl);
        }
    }
    else
    {
        size_t vl = vsetvl_e32m8(N2); // workaround omp loop incr invariance (although it is variant on the final pass)

        #pragma omp parallel for num_threads(threads) firstprivate(vl)
        for (int i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e32m8(N2-i);
            vfloat32m8_t va = vle32_v_f32m8(A + i, vl);
            vfloat32m8_t vb = vle32_v_f32m8(B + i, vl);
            vfloat32m8_t vc = vfadd_vv_f32m8(va, vb, vl);
            vse32_v_f32m8(C + i, vc, vl);
        }
    }

    // end timer
    TIME_GET(&end);

    // free memory
    free(C);
    free(B);
    free(A);
    // compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_add_dpfp(int size, int threads)
{
    // initialize random number generator
    int i = 0;
    srand(SEED);
    TIME_STRUCT start, end;

    // allocate memory for matrices
    size_t N = size;
    size_t N2 = size*size;    

    float64_t* A = aligned_alloc(4096, N2 * sizeof(float64_t));
    float64_t* B = aligned_alloc(4096, N2 * sizeof(float64_t));
    float64_t* C = aligned_alloc(4096, N2 * sizeof(float64_t));

    // initialize input matrices to random numbers
    // initialize output matrix to zeros
    for (i = 0; i < N2; i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
        C[i] = 0;
    }

    //start timer
    TIME_GET(&start);

    // serial operation
    if (threads == 1)
    {
        size_t vl;

        for (i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e64m8(N2-i);

            vfloat64m8_t va = vle64_v_f64m8(A + i, vl);
            vfloat64m8_t vb = vle64_v_f64m8(B + i, vl);
            vfloat64m8_t vc = vfadd_vv_f64m8(va, vb, vl);
            vse64_v_f64m8(C + i, vc, vl);
        }
    }
    else
    {
        size_t vl = vsetvl_e64m8(N2); // workaround omp loop incr invariance (although it is variant on the final pass)

        #pragma omp parallel for num_threads(threads) firstprivate(vl)
        for (i = 0; i < N2; i += vl)
        {
            vl = vsetvl_e64m8(N2-i);
            vfloat64m8_t va = vle64_v_f64m8(A + i, vl);
            vfloat64m8_t vb = vle64_v_f64m8(B + i, vl);
            vfloat64m8_t vc = vfadd_vv_f64m8(va, vb, vl);
            vse64_v_f64m8(C + i, vc, vl);
        }
    }

    //end timer
    TIME_GET(&end);

    //free memory
    free(C);
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_conv_int8(int size, int threads) //unrolled by 6 version
{
    //initialize index variables, random number generator, and timer
    int i, j;

    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
//    INT8_TYPE *A = malloc(sizeof(INT8_TYPE) * ((size + 2) * (size + 2)));
//    INT16_TYPE *B = malloc(sizeof(INT16_TYPE) * (size * size));
	INT8_TYPE *A = aligned_alloc(4096, sizeof(INT8_TYPE) * ((size + 2) * (size + 2)));
	INT16_TYPE *B = aligned_alloc(4096, sizeof(INT16_TYPE) * (size * size));
    //k vector used for sobel filter
    //A vector used for inputs
    int8_t k_vector[3][3];
    //temporary vectors
    //used for holding intermediate values
    //before final result is stored in output
    vint8m2_t r1c1,r1c2,r1c3;	// first row and two shifted copies
    vint8m2_t r2c1,r2c2,r2c3;	// second row and two shifted copies
    vint8m2_t r3c1,r3c2,r3c3;	// third row and two shifted copies
    vint16m4_t result1, result2;
    //initialize the vectors with zeros

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
        B[i] = 0;
    }

    //Initialize the input matrix
    //Add two extra columns and rows of zeros
    //to avoid boundary checking in compuations
    for (i = 0; i < (size + 2); i++)
    {
        for (j = 0; j < (size + 2); j++)
        {
            if (i == 0 || i == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else if (j == 0 || j == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else
            {
                A[i * (size + 2) + j] = rand();
            }
        }
    }


    //initialize convolution kernel to 3x3 Sobel filter
    k_vector[0][0] = 1;
    k_vector[0][1] = 2;
    k_vector[0][2] = 1;
    k_vector[1][0] = 0;
    k_vector[1][1] = 0;
    k_vector[1][2] = 0;
    k_vector[2][0] = -1;
    k_vector[2][1] = -2;
    k_vector[2][2] = -1;

    size_t avl,vl;
    int8_t * Ainpx = (int8_t *)A;
    int8_t * Ainp = (int8_t *)A;
    int16_t * Boutp = (int16_t *)B;
    int16_t * Boutpx = (int16_t *)B;

    //serial operation

    TIME_GET(&start);
#if print_cycles == 1
    uint64_t c0 = rdcycle(), i0 = rdinstret();
#endif
    for (avl=size; avl; avl -= vl)
    {
    	vl = vsetvl_e8m2(avl);
    	Ainp = (int8_t *)Ainpx;
        r1c1 = vle8_v_i8m2 (Ainp, vl);
        r1c2 = vle8_v_i8m2 (Ainp+1, vl);
        r1c3 = vle8_v_i8m2 (Ainp+2, vl);
        Ainp+=size+2; //increment to next row
        r2c1 = vle8_v_i8m2 (Ainp, vl);
        r2c2 = vle8_v_i8m2 (Ainp+1, vl);
        r2c3 = vle8_v_i8m2 (Ainp+2, vl);

        Ainp+=size+2; //increment to next row
        Boutp = Boutpx;
        Ainpx += vl;
        for (i = 0; i < size/6; ++i) //unroll by 6
        {           
		result1= vwmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
		vse16(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse16(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r3c1, k_vector[0][0], vl);
		r2c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmul (r1c1, k_vector[0][0], vl);
		r2c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r3c2, vl);
		r2c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r1c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r3c3, vl);
		r3c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r1c3, vl);
		r3c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r1c1, vl);
		r3c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r2c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r2c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r1c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r2c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r2c3, vl);
		vse16(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r3c3, vl);
		Boutp += size;    
		vse16(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r2c1, k_vector[0][0], vl);
		r1c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmul (r3c1, k_vector[0][0], vl);
		r1c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r2c2, vl);
		r1c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r3c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r2c3, vl);
		r2c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r3c3, vl);
		r2c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r3c1, vl);
		r2c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r1c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r1c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r3c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r1c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r1c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r2c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r2c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r1c3, vl);
		vse16(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r2c3, vl);
		Boutp += size;    
		vse16(Boutp, result2, vl); //Store result into output B matrix
                Boutp += size;     
        }
        //take care of remaining rows if size is not divisible by 6
        switch (size % 6)
        {
            case (0):
        	break;
            case (2):
		result1= vwmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
  
		result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
		vse16(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse16(Boutp, result2, vl); //Store result into output B matrix 

            	break;
            case (4):
		result1= vwmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
		vse16(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse16(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r3c1, k_vector[0][0], vl);
		r2c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmul (r1c1, k_vector[0][0], vl);
		r2c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r3c2, vl);
		r2c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r1c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r3c3, vl);
		r3c1 = vle8_v_i8m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r1c3, vl);
		r3c2 = vle8_v_i8m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r1c1, vl);
		r3c3 = vle8_v_i8m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r2c1, vl);
   
		result1= vwmacc (result1, k_vector[1][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r2c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r1c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r2c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r2c3, vl);
		vse16(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r3c3, vl);
		Boutp += size;    
		vse16(Boutp, result2, vl); //Store result into output B matrix 

                break;
        }
        Boutpx += vl;
    }
#if print_cycles == 1
    uint64_t cycles = rdcycle() - c0;
    uint64_t insts = rdinstret() - i0;
#endif
    TIME_GET(&end);

#if print_cycles == 1
    printf("conv_int8 unroll=6\n");
    printf("Output pixels %d\n",size*size);
    printf("Cycles per pixel *100 %ld\n", cycles*100/(size*size));
    printf("MACs per cycle %ld\n", (size*size*9)/cycles);
    printf("cycles %ld; instret %ld\n\n",cycles, insts);
 #endif       

    //free memory
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_conv_int16(int size, int threads) //unrolled version

{
    //initialize index variables, random number generator, and timer
	int i, j;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
//    INT16_TYPE *A = malloc(sizeof(INT16_TYPE) * ((size + 2) * (size + 2)));
//    INT32_TYPE *B = malloc(sizeof(INT32_TYPE) * (size * size));
	INT16_TYPE *A = aligned_alloc(4096, sizeof(INT16_TYPE) * ((size + 2) * (size + 2)));
	INT32_TYPE *B = aligned_alloc(4096, sizeof(INT32_TYPE) * (size * size));
    //k vector used for sobel filter
    //A vector used for inputs
    int16_t k_vector[3][3];

    //temporary vectors
    //used for holding intermediate values
    //before final result is stored in output
    vint16m2_t r1c1,r1c2,r1c3;	// first row and two shifted copies
    vint16m2_t r2c1,r2c2,r2c3;	// second row and two shifted copies
    vint16m2_t r3c1,r3c2,r3c3;	// third row and two shifted copies
    vint32m4_t result1, result2;	//accumulators
    //initialize the vectors with zeros



    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
        B[i] = 0;
    }

    //Initialize the input matrix
    //Add two extra columns and rows of zeros
    //to avoid boundary checking in compuations
    for (i = 0; i < (size + 2); i++)
    {
        for (j = 0; j < (size + 2); j++)
        {
            if (i == 0 || i == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else if (j == 0 || j == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else
            {
                A[i * (size + 2) + j] = rand();
            }
        }
    }

    //initialize convolution kernel to 3x3 Sobel filter
    k_vector[0][0] = 1;
    k_vector[0][1] = 2;
    k_vector[0][2] = 1;
    k_vector[1][0] = 0;
    k_vector[1][1] = 0;
    k_vector[1][2] = 0;
    k_vector[2][0] = -1;
    k_vector[2][1] = -2;
    k_vector[2][2] = -1;

    size_t avl,vl;
    int16_t * Ainpx = (int16_t *)A;
    int16_t * Ainp = (int16_t *)A;
    int32_t * Boutp = (int32_t *)B;
    int32_t * Boutpx = (int32_t *)B;
    TIME_GET(&start);
#if print_cycles == 1
    uint64_t c0 = rdcycle(), i0 = rdinstret();
#endif
    for (avl=size; avl; avl -= vl)
    {
    	vl = vsetvl_e16m2(avl);
    	Ainp = (int16_t *)Ainpx;
        r1c1 = vle16_v_i16m2 (Ainp, vl);
        r1c2 = vle16_v_i16m2 (Ainp+1, vl);
        r1c3 = vle16_v_i16m2 (Ainp+2, vl);
        Ainp+=size+2; //increment to next row
        r2c1 = vle16_v_i16m2 (Ainp, vl);
        r2c2 = vle16_v_i16m2 (Ainp+1, vl);
        r2c3 = vle16_v_i16m2 (Ainp+2, vl);
        Ainp+=size+2; //increment to next row
        Boutp = Boutpx;
        Ainpx += vl;
        for (i = 0; i < size/6; ++i) //unroll by 6
        {
		result1= vwmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle16_v_i16m2 (Ainp, vl);
		result2= vwmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle16_v_i16m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle16_v_i16m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle16_v_i16m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle16_v_i16m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle16_v_i16m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
		vse32(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse32(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r3c1, k_vector[0][0], vl);
		r2c1 = vle16_v_i16m2 (Ainp, vl);
		result2= vwmul (r1c1, k_vector[0][0], vl);
		r2c2 = vle16_v_i16m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r3c2, vl);
		r2c3 = vle16_v_i16m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r1c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r3c3, vl);
		r3c1 = vle16_v_i16m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r1c3, vl);
		r3c2 = vle16_v_i16m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r1c1, vl);
		r3c3 = vle16_v_i16m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r2c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r2c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r1c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r2c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r2c3, vl);
		vse32(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r3c3, vl);
		Boutp += size;    
		vse32(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r2c1, k_vector[0][0], vl);
		r1c1 = vle16_v_i16m2 (Ainp, vl);
		result2= vwmul (r3c1, k_vector[0][0], vl);
		r1c2 = vle16_v_i16m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r2c2, vl);
		r1c3 = vle16_v_i16m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r3c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r2c3, vl);
		r2c1 = vle16_v_i16m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r3c3, vl);
		r2c2 = vle16_v_i16m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r3c1, vl);
		r2c3 = vle16_v_i16m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r1c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r1c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r3c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r1c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r1c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r2c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r2c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r1c3, vl);
		vse32(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r2c3, vl);
		Boutp += size;    
		vse32(Boutp, result2, vl); //Store result into output B matrix
                Boutp += size;     

        }
            //take care of remaining rows if size is not divisible by 6
            switch (size % 6)
            {
            	case (0):
            	break;
                case (2):
			result1= vwmul (r1c1, k_vector[0][0], vl);
			r3c1 = vle16_v_i16m2 (Ainp, vl);
			result2= vwmul (r2c1, k_vector[0][0], vl);
			r3c2 = vle16_v_i16m2 (Ainp+1, vl);
			result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
			r3c3 = vle16_v_i16m2 (Ainp+2, vl);
			result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
			r1c1 = vle16_v_i16m2 (Ainp, vl);
			result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
			r1c2 = vle16_v_i16m2 (Ainp+1, vl);
			result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
			r1c3 = vle16_v_i16m2 (Ainp+2, vl);
			result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
   
			result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
			result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
			result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
			result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
			result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
			result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
			result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
			result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
			result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
			vse32(Boutp, result1, vl); //Store result into output B matrix
			result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
			Boutp += size;    
			vse32(Boutp, result2, vl); //Store result into output B matrix 

                break;
                case (4):
			result1= vwmul (r1c1, k_vector[0][0], vl);
			r3c1 = vle16_v_i16m2 (Ainp, vl);
			result2= vwmul (r2c1, k_vector[0][0], vl);
			r3c2 = vle16_v_i16m2 (Ainp+1, vl);
			result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
			r3c3 = vle16_v_i16m2 (Ainp+2, vl);
			result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
			r1c1 = vle16_v_i16m2 (Ainp, vl);
			result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
			r1c2 = vle16_v_i16m2 (Ainp+1, vl);
			result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
			r1c3 = vle16_v_i16m2 (Ainp+2, vl);
			result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
			Ainp+=size+2; //increment to next row    
			result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
			result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
			result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
			result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
			result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
			result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
			result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
			result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
			result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
			vse32(Boutp, result1, vl); //Store result into output B matrix
			result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
			Boutp += size;    
			vse32(Boutp, result2, vl); //Store result into output B matrix 
			Boutp += size;

			result1= vwmul (r3c1, k_vector[0][0], vl);
			r2c1 = vle16_v_i16m2 (Ainp, vl);
			result2= vwmul (r1c1, k_vector[0][0], vl);
			r2c2 = vle16_v_i16m2 (Ainp+1, vl);
			result1= vwmacc (result1, k_vector[0][1], r3c2, vl);
			r2c3 = vle16_v_i16m2 (Ainp+2, vl);
			result2= vwmacc (result2, k_vector[0][1], r1c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vwmacc (result1, k_vector[0][2], r3c3, vl);
			r3c1 = vle16_v_i16m2 (Ainp, vl);
			result2= vwmacc (result2, k_vector[0][2], r1c3, vl);
			r3c2 = vle16_v_i16m2 (Ainp+1, vl);
			result1= vwmacc (result1, k_vector[1][0], r1c1, vl);
			r3c3 = vle16_v_i16m2 (Ainp+2, vl);
			result2= vwmacc (result2, k_vector[1][0], r2c1, vl);
   
			result1= vwmacc (result1, k_vector[1][1], r1c2, vl);
			result2= vwmacc (result2, k_vector[1][1], r2c2, vl);	    
			result1= vwmacc (result1, k_vector[1][2], r1c3, vl);
			result2= vwmacc (result2, k_vector[1][2], r2c3, vl);            
			result1= vwmacc (result1, k_vector[2][0], r2c2, vl);
			result2= vwmacc (result2, k_vector[2][0], r3c2, vl);
			result1= vwmacc (result1, k_vector[2][1], r2c2, vl);
			result2= vwmacc (result2, k_vector[2][1], r3c2, vl);
			result1= vwmacc (result1, k_vector[2][2], r2c3, vl);
			vse32(Boutp, result1, vl); //Store result into output B matrix
			result2= vwmacc (result2, k_vector[2][2], r3c3, vl);
			Boutp += size;    
			vse32(Boutp, result2, vl); //Store result into output B matrix 

                 break;

        }
        Boutpx += vl;
    }


#if print_cycles == 1
    uint64_t cycles = rdcycle() - c0;
    uint64_t insts = rdinstret() - i0;
#endif
    TIME_GET(&end);

#if print_cycles == 1
    printf("conv_int16 unroll=6\n");
    printf("Output pixels %d\n",size*size);
    printf("Cycles per pixel *100 %ld\n", cycles*100/(size*size));
    printf("MACs per cycle %ld\n", (size*size*9)/cycles);
    printf("cycles %ld; instret %ld\n\n",cycles, insts);
 #endif 


    //free memory
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_conv_int32(int size, int threads) //unrolled by 6 version
{
    //initialize index variables, random number generator, and timer
    int i, j;
    int kSize = 3;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
//    INT32_TYPE *A = malloc(sizeof(INT32_TYPE) * ((size + 2) * (size + 2)));
//    INT64_TYPE *B = malloc(sizeof(INT64_TYPE) * (size * size));
	INT32_TYPE *A = aligned_alloc(4096, sizeof(INT32_TYPE) * ((size + 2) * (size + 2)));
	INT64_TYPE *B = aligned_alloc(4096, sizeof(INT64_TYPE) * (size * size));
    int32_t k_vector[3][3];
    //temporary vector
    //used for holding intermediate values
    //before final result is stored in output

    vint32m2_t r1c1,r1c2,r1c3;	// first row and two shifted copies
	vint32m2_t r2c1,r2c2,r2c3;	// second row and two shifted copies
	vint32m2_t r3c1,r3c2,r3c3;	// second row and two shifted copies
	vint64m4_t result1, result2;
    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
        B[i] = 0;
    }

    //Initialize the input matrix
    //Add two extra columns and rows of zeros
    //to avoid boundary checking in compuations
    for (i = 0; i < (size + 2); i++)
    {
        for (j = 0; j < (size + 2); j++)
        {
            if (i == 0 || i == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else if (j == 0 || j == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else
            {
                A[i * (size + 2) + j] = i;
            }
        }
    }

    //initialize convolution kernel to 3x3 Sobel filter
    k_vector[0][0] = 1;
    k_vector[0][1] = 2;
    k_vector[0][2] = 1;
    k_vector[1][0] = 0;
    k_vector[1][1] = 0;
    k_vector[1][2] = 0;
    k_vector[2][0] = -1;
    k_vector[2][1] = -2;
    k_vector[2][2] = -1;

    //serial operation

    size_t avl,vl;
    int32_t * Ainpx = (int32_t *)A;
    int32_t * Ainp = (int32_t *)A;
    int64_t * Boutp = (int64_t *)B;
    int64_t * Boutpx = (int64_t *)B;
    TIME_GET(&start);
#if print_cycles == 1
    uint64_t c0 = rdcycle(), i0 = rdinstret();
#endif
    for (avl=size; avl; avl -= vl)
    {
    	vl = vsetvl_e32m2(avl);
    	Ainp = (int32_t *)Ainpx;
        r1c1 = vle32_v_i32m2 (Ainp, vl);
        r1c2 = vle32_v_i32m2 (Ainp+1, vl);
        r1c3 = vle32_v_i32m2 (Ainp+2, vl);
        Ainp+=size+2; //increment to next row
        r2c1 = vle32_v_i32m2 (Ainp, vl);
        r2c2 = vle32_v_i32m2 (Ainp+1, vl);
        r2c3 = vle32_v_i32m2 (Ainp+2, vl);
        Ainp+=size+2; //increment to next row
        Boutp = Boutpx;
        Ainpx += vl;
        for (i = 0; i < size/6; ++i) //unroll by 6
        {


		result1= vwmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r3c1, k_vector[0][0], vl);
		r2c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmul (r1c1, k_vector[0][0], vl);
		r2c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r3c2, vl);
		r2c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r1c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r3c3, vl);
		r3c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r1c3, vl);
		r3c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r1c1, vl);
		r3c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r2c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r2c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r1c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r2c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r2c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r3c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r2c1, k_vector[0][0], vl);
		r1c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmul (r3c1, k_vector[0][0], vl);
		r1c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r2c2, vl);
		r1c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r3c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r2c3, vl);
		r2c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r3c3, vl);
		r2c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r3c1, vl);
		r2c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r1c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r1c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r3c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r1c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r1c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r2c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r2c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r1c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r2c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix
                Boutp += size;     

        }

        //take care of remaining rows if size is not divisible by 3
        switch (size % 6)
        {
        	case (0):
        			break;
            case (2):
		result1= vwmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
   
		result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix 

            		break;
            case (4):
		result1= vwmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r3c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vwmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r3c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vwmul (r3c1, k_vector[0][0], vl);
		r2c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmul (r1c1, k_vector[0][0], vl);
		r2c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[0][1], r3c2, vl);
		r2c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[0][1], r1c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vwmacc (result1, k_vector[0][2], r3c3, vl);
		r3c1 = vle32_v_i32m2 (Ainp, vl);
		result2= vwmacc (result2, k_vector[0][2], r1c3, vl);
		r3c2 = vle32_v_i32m2 (Ainp+1, vl);
		result1= vwmacc (result1, k_vector[1][0], r1c1, vl);
		r3c3 = vle32_v_i32m2 (Ainp+2, vl);
		result2= vwmacc (result2, k_vector[1][0], r2c1, vl);
  
		result1= vwmacc (result1, k_vector[1][1], r1c2, vl);
		result2= vwmacc (result2, k_vector[1][1], r2c2, vl);	    
		result1= vwmacc (result1, k_vector[1][2], r1c3, vl);
		result2= vwmacc (result2, k_vector[1][2], r2c3, vl);            
		result1= vwmacc (result1, k_vector[2][0], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][0], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][1], r2c2, vl);
		result2= vwmacc (result2, k_vector[2][1], r3c2, vl);
		result1= vwmacc (result1, k_vector[2][2], r2c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vwmacc (result2, k_vector[2][2], r3c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix 

        	}
        Boutpx += vl;
    }


#if print_cycles == 1
    uint64_t cycles = rdcycle() - c0;
    uint64_t insts = rdinstret() - i0;
#endif
    TIME_GET(&end);

#if print_cycles == 1
    printf("conv_int32 unroll=6\n");
    printf("Output pixels %d\n",size*size);
    printf("Cycles per pixel *100 %ld\n", cycles*100/(size*size));
    printf("MACs per cycle %ld\n", (size*size*9)/cycles);
    printf("cycles %ld; instret %ld\n\n",cycles, insts);
 #endif 

    //parallel operation



    //free memory
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_conv_spfp(int size, int threads) //unrolled by 6 version
{
    //initialize index variables, random number generator, and timer
    int i, j;
    int kSize = 3;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
//    SPFP_TYPE *A = malloc(sizeof(SPFP_TYPE) * ((size + 2) * (size + 2)));
 //   SPFP_TYPE *B = malloc(sizeof(SPFP_TYPE) * (size * size));
	SPFP_TYPE *A = aligned_alloc(4096, sizeof(SPFP_TYPE) * ((size + 2) * (size + 2)));
	SPFP_TYPE *B = aligned_alloc(4096, sizeof(SPFP_TYPE) * (size * size));
    //k vector used for sobel filter
    //A vector used for inputs
    float32_t k_vector[3][3];
    //temporary vectors
    //used for holding intermediate values
    //before final result is stored in output
    vfloat32m2_t r1c1,r1c2,r1c3;	// first row and two shifted copies
    vfloat32m2_t r2c1,r2c2,r2c3;	// second row and two shifted copies
    vfloat32m2_t r3c1,r3c2,r3c3;	// second row and two shifted copies
    vfloat32m2_t result1, result2;

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
        B[i] = 0;
    }

    //Initialize the input matrix
    //Add two extra columns and rows of zeros
    //to avoid boundary checking in compuations
    for (i = 0; i < (size + 2); i++)
    {
        for (j = 0; j < (size + 2); j++)
        {
            if (i == 0 || i == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else if (j == 0 || j == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else
            {
                A[i * (size + 2) + j] = rand();
            }
        }
    }

    //initialize convolution kernel to 3x3 Sobel filter
    k_vector[0][0] = 1;
    k_vector[0][1] = 2;
    k_vector[0][2] = 1;
    k_vector[1][0] = 0;
    k_vector[1][1] = 0;
    k_vector[1][2] = 0;
    k_vector[2][0] = -1;
    k_vector[2][1] = -2;
    k_vector[2][2] = -1;

    //serial operation

    size_t avl,vl;
    float32_t * Ainpx = (float32_t *)A;
    float32_t * Ainp = (float32_t *)A;
    float32_t * Boutp = (float32_t *)B;
    float32_t * Boutpx = (float32_t *)B;
        TIME_GET(&start);
#if print_cycles == 1
    uint64_t c0 = rdcycle(), i0 = rdinstret();
#endif
        for (avl=size; avl; avl -= vl)
        {
        	vl = vsetvl_e32m2(avl);
        	Ainp = (float32_t *)Ainpx;
            r1c1 = vle32_v_f32m2 (Ainp, vl);
            r1c2 = vle32_v_f32m2 (Ainp+1, vl);
            r1c3 = vle32_v_f32m2 (Ainp+2, vl);
            Ainp+=size+2; //increment to next row
            r2c1 = vle32_v_f32m2 (Ainp, vl);
            r2c2 = vle32_v_f32m2 (Ainp+1, vl);
            r2c3 = vle32_v_f32m2 (Ainp+2, vl);
            Ainp+=size+2; //increment to next row
            Boutp = Boutpx;
            Ainpx += vl;
            for (i = 0; i < size/6; ++i) //unroll by 6
            {

		result1= vfmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle32_v_f32m2 (Ainp, vl);
		result2= vfmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle32_v_f32m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle32_v_f32m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vfmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle32_v_f32m2 (Ainp, vl);
		result2= vfmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle32_v_f32m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle32_v_f32m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[1][0], r3c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vfmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vfmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vfmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vfmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vfmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vfmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vfmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vfmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vfmacc (result1, k_vector[2][2], r3c3, vl);
		vse32(Boutp, result1, vl); //Store result into output B matrix
		result2= vfmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse32(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vfmul (r3c1, k_vector[0][0], vl);
		r2c1 = vle32_v_f32m2 (Ainp, vl);
		result2= vfmul (r1c1, k_vector[0][0], vl);
		r2c2 = vle32_v_f32m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[0][1], r3c2, vl);
		r2c3 = vle32_v_f32m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[0][1], r1c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vfmacc (result1, k_vector[0][2], r3c3, vl);
		r3c1 = vle32_v_f32m2 (Ainp, vl);
		result2= vfmacc (result2, k_vector[0][2], r1c3, vl);
		r3c2 = vle32_v_f32m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[1][0], r1c1, vl);
		r3c3 = vle32_v_f32m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[1][0], r2c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vfmacc (result1, k_vector[1][1], r1c2, vl);
		result2= vfmacc (result2, k_vector[1][1], r2c2, vl);	    
		result1= vfmacc (result1, k_vector[1][2], r1c3, vl);
		result2= vfmacc (result2, k_vector[1][2], r2c3, vl);            
		result1= vfmacc (result1, k_vector[2][0], r2c2, vl);
		result2= vfmacc (result2, k_vector[2][0], r3c2, vl);
		result1= vfmacc (result1, k_vector[2][1], r2c2, vl);
		result2= vfmacc (result2, k_vector[2][1], r3c2, vl);
		result1= vfmacc (result1, k_vector[2][2], r2c3, vl);
		vse32(Boutp, result1, vl); //Store result into output B matrix
		result2= vfmacc (result2, k_vector[2][2], r3c3, vl);
		Boutp += size;    
		vse32(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vfmul (r2c1, k_vector[0][0], vl);
		r1c1 = vle32_v_f32m2 (Ainp, vl);
		result2= vfmul (r3c1, k_vector[0][0], vl);
		r1c2 = vle32_v_f32m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[0][1], r2c2, vl);
		r1c3 = vle32_v_f32m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[0][1], r3c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vfmacc (result1, k_vector[0][2], r2c3, vl);
		r2c1 = vle32_v_f32m2 (Ainp, vl);
		result2= vfmacc (result2, k_vector[0][2], r3c3, vl);
		r2c2 = vle32_v_f32m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[1][0], r3c1, vl);
		r2c3 = vle32_v_f32m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[1][0], r1c1, vl);
  		Ainp+=size+2; //increment to next row 
		result1= vfmacc (result1, k_vector[1][1], r3c2, vl);
		result2= vfmacc (result2, k_vector[1][1], r1c2, vl);	    
		result1= vfmacc (result1, k_vector[1][2], r3c3, vl);
		result2= vfmacc (result2, k_vector[1][2], r1c3, vl);            
		result1= vfmacc (result1, k_vector[2][0], r1c2, vl);
		result2= vfmacc (result2, k_vector[2][0], r2c2, vl);
		result1= vfmacc (result1, k_vector[2][1], r1c2, vl);
		result2= vfmacc (result2, k_vector[2][1], r2c2, vl);
		result1= vfmacc (result1, k_vector[2][2], r1c3, vl);
		vse32(Boutp, result1, vl); //Store result into output B matrix
		result2= vfmacc (result2, k_vector[2][2], r2c3, vl);
		Boutp += size;    
		vse32(Boutp, result2, vl); //Store result into output B matrix
		Boutp += size; 
            }

            //take care of remaining rows if size is not divisible by 3
            switch (size % 6)
            {
            	case (0):
            			break;
                case (2):
			result1= vfmul (r1c1, k_vector[0][0], vl);
			r3c1 = vle32_v_f32m2 (Ainp, vl);
			result2= vfmul (r2c1, k_vector[0][0], vl);
			r3c2 = vle32_v_f32m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[0][1], r1c2, vl);
			r3c3 = vle32_v_f32m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[0][1], r2c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vfmacc (result1, k_vector[0][2], r1c3, vl);
			r1c1 = vle32_v_f32m2 (Ainp, vl);
			result2= vfmacc (result2, k_vector[0][2], r2c3, vl);
			r1c2 = vle32_v_f32m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[1][0], r2c1, vl);
			r1c3 = vle32_v_f32m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[1][0], r3c1, vl);  
			result1= vfmacc (result1, k_vector[1][1], r2c2, vl);
			result2= vfmacc (result2, k_vector[1][1], r3c2, vl);	    
			result1= vfmacc (result1, k_vector[1][2], r2c3, vl);
			result2= vfmacc (result2, k_vector[1][2], r3c3, vl);            
			result1= vfmacc (result1, k_vector[2][0], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][0], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][1], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][1], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][2], r3c3, vl);
			vse32(Boutp, result1, vl); //Store result into output B matrix
			result2= vfmacc (result2, k_vector[2][2], r1c3, vl);
			Boutp += size;    
			vse32(Boutp, result2, vl); //Store result into output B matrix 

					break;
                case (4):
			result1= vfmul (r1c1, k_vector[0][0], vl);
			r3c1 = vle32_v_f32m2 (Ainp, vl);
			result2= vfmul (r2c1, k_vector[0][0], vl);
			r3c2 = vle32_v_f32m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[0][1], r1c2, vl);
			r3c3 = vle32_v_f32m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[0][1], r2c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vfmacc (result1, k_vector[0][2], r1c3, vl);
			r1c1 = vle32_v_f32m2 (Ainp, vl);
			result2= vfmacc (result2, k_vector[0][2], r2c3, vl);
			r1c2 = vle32_v_f32m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[1][0], r2c1, vl);
			r1c3 = vle32_v_f32m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[1][0], r3c1, vl);
			Ainp+=size+2; //increment to next row    
			result1= vfmacc (result1, k_vector[1][1], r2c2, vl);
			result2= vfmacc (result2, k_vector[1][1], r3c2, vl);	    
			result1= vfmacc (result1, k_vector[1][2], r2c3, vl);
			result2= vfmacc (result2, k_vector[1][2], r3c3, vl);            
			result1= vfmacc (result1, k_vector[2][0], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][0], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][1], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][1], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][2], r3c3, vl);
			vse32(Boutp, result1, vl); //Store result into output B matrix
			result2= vfmacc (result2, k_vector[2][2], r1c3, vl);
			Boutp += size;    
			vse32(Boutp, result2, vl); //Store result into output B matrix 
			Boutp += size;

			result1= vfmul (r3c1, k_vector[0][0], vl);
			r2c1 = vle32_v_f32m2 (Ainp, vl);
			result2= vfmul (r1c1, k_vector[0][0], vl);
			r2c2 = vle32_v_f32m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[0][1], r3c2, vl);
			r2c3 = vle32_v_f32m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[0][1], r1c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vfmacc (result1, k_vector[0][2], r3c3, vl);
			r3c1 = vle32_v_f32m2 (Ainp, vl);
			result2= vfmacc (result2, k_vector[0][2], r1c3, vl);
			r3c2 = vle32_v_f32m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[1][0], r1c1, vl);
			r3c3 = vle32_v_f32m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[1][0], r2c1, vl);
  
			result1= vfmacc (result1, k_vector[1][1], r1c2, vl);
			result2= vfmacc (result2, k_vector[1][1], r2c2, vl);	    
			result1= vfmacc (result1, k_vector[1][2], r1c3, vl);
			result2= vfmacc (result2, k_vector[1][2], r2c3, vl);            
			result1= vfmacc (result1, k_vector[2][0], r2c2, vl);
			result2= vfmacc (result2, k_vector[2][0], r3c2, vl);
			result1= vfmacc (result1, k_vector[2][1], r2c2, vl);
			result2= vfmacc (result2, k_vector[2][1], r3c2, vl);
			result1= vfmacc (result1, k_vector[2][2], r2c3, vl);
			vse32(Boutp, result1, vl); //Store result into output B matrix
			result2= vfmacc (result2, k_vector[2][2], r3c3, vl);
			Boutp += size;    
			vse32(Boutp, result2, vl); //Store result into output B matrix 
            	}
            Boutpx += vl;
        }
 #if print_cycles == 1
    uint64_t cycles = rdcycle() - c0;
    uint64_t insts = rdinstret() - i0;
#endif
    TIME_GET(&end);

#if print_cycles == 1
    printf("conv_spfp unroll=6\n");
    printf("Output pixels %d\n",size*size);
    printf("Cycles per pixel *100 %ld\n", cycles*100/(size*size));
    printf("MACs per cycle %ld\n", (size*size*9)/cycles);
    printf("cycles %ld; instret %ld\n\n",cycles, insts);
 #endif 

    //parallel operation

    //free memory
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_conv_dpfp(int size, int threads) //unrolled version
{
    //initialize index variables, random number generator, and timer
    int i, j;

    int kSize = 3;

    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
//    DPFP_TYPE *A = malloc(sizeof(DPFP_TYPE) * ((size + 2) * (size + 2)));
//    DPFP_TYPE *B = malloc(sizeof(DPFP_TYPE) * (size * size));
	DPFP_TYPE *A = aligned_alloc(4096, sizeof(DPFP_TYPE) * ((size + 2) * (size + 2)));
	DPFP_TYPE *B = aligned_alloc(4096, sizeof(DPFP_TYPE) * (size * size));
    //k vector used for sobel filter
    //A vector used for inputs
    float64_t k_vector[3][3];

    //temporary vectors
    //used for holding intermediate values
    //before final result is stored in output
    vfloat64m2_t r1c1,r1c2,r1c3;	// first row and two shifted copies
    vfloat64m2_t r2c1,r2c2,r2c3;	// second row and two shifted copies
    vfloat64m2_t r3c1,r3c2,r3c3;	// second row and two shifted copies
    vfloat64m2_t result1,result2;

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
        B[i] = 0;
    }

    //Initialize the input matrix
    //Add two extra columns and rows of zeros
    //to avoid boundary checking in compuations
    for (i = 0; i < (size + 2); i++)
    {
        for (j = 0; j < (size + 2); j++)
        {
            if (i == 0 || i == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else if (j == 0 || j == (size + 2 - 1))
            {
                A[i * (size + 2) + j] = 0;
            }
            else
            {
                A[i * (size + 2) + j] = rand();
            }
        }
    }

    //initialize convolution kernel to 3x3 Sobel filter
    k_vector[0][0] = 1;
    k_vector[0][1] = 2;
    k_vector[0][2] = 1;
    k_vector[1][0] = 0;
    k_vector[1][1] = 0;
    k_vector[1][2] = 0;
    k_vector[2][0] = -1;
    k_vector[2][1] = -2;
    k_vector[2][2] = -1;

    //serial operation

    size_t avl,vl;
    float64_t * Ainpx = (float64_t *)A;
    float64_t * Ainp = (float64_t *)A;
    float64_t * Boutp = (float64_t *)B;
    float64_t * Boutpx = (float64_t *)B;
        TIME_GET(&start);
#if print_cycles == 1
    uint64_t c0 = rdcycle(), i0 = rdinstret();
#endif
        for (avl=size; avl; avl -= vl)
        {
        	vl = vsetvl_e64m2(avl);
        	Ainp = (float64_t *)Ainpx;
            r1c1 = vle64_v_f64m2 (Ainp, vl);
            r1c2 = vle64_v_f64m2 (Ainp+1, vl);
            r1c3 = vle64_v_f64m2 (Ainp+2, vl);
            Ainp+=size+2; //increment to next row
            r2c1 = vle64_v_f64m2 (Ainp, vl);
            r2c2 = vle64_v_f64m2 (Ainp+1, vl);
            r2c3 = vle64_v_f64m2 (Ainp+2, vl);
            Ainp+=size+2; //increment to next row
            Boutp = Boutpx;
            Ainpx += vl;
            for (i = 0; i < size/6; ++i)	//unroll by 6
            {
		result1= vfmul (r1c1, k_vector[0][0], vl);
		r3c1 = vle64_v_f64m2 (Ainp, vl);
		result2= vfmul (r2c1, k_vector[0][0], vl);
		r3c2 = vle64_v_f64m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[0][1], r1c2, vl);
		r3c3 = vle64_v_f64m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[0][1], r2c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vfmacc (result1, k_vector[0][2], r1c3, vl);
		r1c1 = vle64_v_f64m2 (Ainp, vl);
		result2= vfmacc (result2, k_vector[0][2], r2c3, vl);
		r1c2 = vle64_v_f64m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[1][0], r2c1, vl);
		r1c3 = vle64_v_f64m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[1][0], r3c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vfmacc (result1, k_vector[1][1], r2c2, vl);
		result2= vfmacc (result2, k_vector[1][1], r3c2, vl);	    
		result1= vfmacc (result1, k_vector[1][2], r2c3, vl);
		result2= vfmacc (result2, k_vector[1][2], r3c3, vl);            
		result1= vfmacc (result1, k_vector[2][0], r3c2, vl);
		result2= vfmacc (result2, k_vector[2][0], r1c2, vl);
		result1= vfmacc (result1, k_vector[2][1], r3c2, vl);
		result2= vfmacc (result2, k_vector[2][1], r1c2, vl);
		result1= vfmacc (result1, k_vector[2][2], r3c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vfmacc (result2, k_vector[2][2], r1c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vfmul (r3c1, k_vector[0][0], vl);
		r2c1 = vle64_v_f64m2 (Ainp, vl);
		result2= vfmul (r1c1, k_vector[0][0], vl);
		r2c2 = vle64_v_f64m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[0][1], r3c2, vl);
		r2c3 = vle64_v_f64m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[0][1], r1c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vfmacc (result1, k_vector[0][2], r3c3, vl);
		r3c1 = vle64_v_f64m2 (Ainp, vl);
		result2= vfmacc (result2, k_vector[0][2], r1c3, vl);
		r3c2 = vle64_v_f64m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[1][0], r1c1, vl);
		r3c3 = vle64_v_f64m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[1][0], r2c1, vl);
		Ainp+=size+2; //increment to next row    
		result1= vfmacc (result1, k_vector[1][1], r1c2, vl);
		result2= vfmacc (result2, k_vector[1][1], r2c2, vl);	    
		result1= vfmacc (result1, k_vector[1][2], r1c3, vl);
		result2= vfmacc (result2, k_vector[1][2], r2c3, vl);            
		result1= vfmacc (result1, k_vector[2][0], r2c2, vl);
		result2= vfmacc (result2, k_vector[2][0], r3c2, vl);
		result1= vfmacc (result1, k_vector[2][1], r2c2, vl);
		result2= vfmacc (result2, k_vector[2][1], r3c2, vl);
		result1= vfmacc (result1, k_vector[2][2], r2c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vfmacc (result2, k_vector[2][2], r3c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix 
		Boutp += size;

		result1= vfmul (r2c1, k_vector[0][0], vl);
		r1c1 = vle64_v_f64m2 (Ainp, vl);
		result2= vfmul (r3c1, k_vector[0][0], vl);
		r1c2 = vle64_v_f64m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[0][1], r2c2, vl);
		r1c3 = vle64_v_f64m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[0][1], r3c2, vl);
		Ainp+=size+2; //increment to next row	 
		result1= vfmacc (result1, k_vector[0][2], r2c3, vl);
		r2c1 = vle64_v_f64m2 (Ainp, vl);
		result2= vfmacc (result2, k_vector[0][2], r3c3, vl);
		r2c2 = vle64_v_f64m2 (Ainp+1, vl);
		result1= vfmacc (result1, k_vector[1][0], r3c1, vl);
		r2c3 = vle64_v_f64m2 (Ainp+2, vl);
		result2= vfmacc (result2, k_vector[1][0], r1c1, vl);
  		Ainp+=size+2; //increment to next row 
		result1= vfmacc (result1, k_vector[1][1], r3c2, vl);
		result2= vfmacc (result2, k_vector[1][1], r1c2, vl);	    
		result1= vfmacc (result1, k_vector[1][2], r3c3, vl);
		result2= vfmacc (result2, k_vector[1][2], r1c3, vl);            
		result1= vfmacc (result1, k_vector[2][0], r1c2, vl);
		result2= vfmacc (result2, k_vector[2][0], r2c2, vl);
		result1= vfmacc (result1, k_vector[2][1], r1c2, vl);
		result2= vfmacc (result2, k_vector[2][1], r2c2, vl);
		result1= vfmacc (result1, k_vector[2][2], r1c3, vl);
		vse64(Boutp, result1, vl); //Store result into output B matrix
		result2= vfmacc (result2, k_vector[2][2], r2c3, vl);
		Boutp += size;    
		vse64(Boutp, result2, vl); //Store result into output B matrix
		Boutp += size; 


            }
            //take care of remaining rows if size is not divisible by 6
            switch (size % 6)
            {
            	case (0):
            			break;
                case (2):
			result1= vfmul (r1c1, k_vector[0][0], vl);
			r3c1 = vle64_v_f64m2 (Ainp, vl);
			result2= vfmul (r2c1, k_vector[0][0], vl);
			r3c2 = vle64_v_f64m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[0][1], r1c2, vl);
			r3c3 = vle64_v_f64m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[0][1], r2c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vfmacc (result1, k_vector[0][2], r1c3, vl);
			r1c1 = vle64_v_f64m2 (Ainp, vl);
			result2= vfmacc (result2, k_vector[0][2], r2c3, vl);
			r1c2 = vle64_v_f64m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[1][0], r2c1, vl);
			r1c3 = vle64_v_f64m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[1][0], r3c1, vl);
 
			result1= vfmacc (result1, k_vector[1][1], r2c2, vl);
			result2= vfmacc (result2, k_vector[1][1], r3c2, vl);	    
			result1= vfmacc (result1, k_vector[1][2], r2c3, vl);
			result2= vfmacc (result2, k_vector[1][2], r3c3, vl);            
			result1= vfmacc (result1, k_vector[2][0], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][0], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][1], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][1], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][2], r3c3, vl);
			vse64(Boutp, result1, vl); //Store result into output B matrix
			result2= vfmacc (result2, k_vector[2][2], r1c3, vl);
			Boutp += size;    
			vse64(Boutp, result2, vl); //Store result into output B matrix 
					break;
                case (4):
			result1= vfmul (r1c1, k_vector[0][0], vl);
			r3c1 = vle64_v_f64m2 (Ainp, vl);
			result2= vfmul (r2c1, k_vector[0][0], vl);
			r3c2 = vle64_v_f64m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[0][1], r1c2, vl);
			r3c3 = vle64_v_f64m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[0][1], r2c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vfmacc (result1, k_vector[0][2], r1c3, vl);
			r1c1 = vle64_v_f64m2 (Ainp, vl);
			result2= vfmacc (result2, k_vector[0][2], r2c3, vl);
			r1c2 = vle64_v_f64m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[1][0], r2c1, vl);
			r1c3 = vle64_v_f64m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[1][0], r3c1, vl);
			Ainp+=size+2; //increment to next row    
			result1= vfmacc (result1, k_vector[1][1], r2c2, vl);
			result2= vfmacc (result2, k_vector[1][1], r3c2, vl);	    
			result1= vfmacc (result1, k_vector[1][2], r2c3, vl);
			result2= vfmacc (result2, k_vector[1][2], r3c3, vl);            
			result1= vfmacc (result1, k_vector[2][0], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][0], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][1], r3c2, vl);
			result2= vfmacc (result2, k_vector[2][1], r1c2, vl);
			result1= vfmacc (result1, k_vector[2][2], r3c3, vl);
			vse64(Boutp, result1, vl); //Store result into output B matrix
			result2= vfmacc (result2, k_vector[2][2], r1c3, vl);
			Boutp += size;    
			vse64(Boutp, result2, vl); //Store result into output B matrix 
			Boutp += size;

			result1= vfmul (r3c1, k_vector[0][0], vl);
			r2c1 = vle64_v_f64m2 (Ainp, vl);
			result2= vfmul (r1c1, k_vector[0][0], vl);
			r2c2 = vle64_v_f64m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[0][1], r3c2, vl);
			r2c3 = vle64_v_f64m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[0][1], r1c2, vl);
			Ainp+=size+2; //increment to next row	 
			result1= vfmacc (result1, k_vector[0][2], r3c3, vl);
			r3c1 = vle64_v_f64m2 (Ainp, vl);
			result2= vfmacc (result2, k_vector[0][2], r1c3, vl);
			r3c2 = vle64_v_f64m2 (Ainp+1, vl);
			result1= vfmacc (result1, k_vector[1][0], r1c1, vl);
			r3c3 = vle64_v_f64m2 (Ainp+2, vl);
			result2= vfmacc (result2, k_vector[1][0], r2c1, vl);
	  
			result1= vfmacc (result1, k_vector[1][1], r1c2, vl);
			result2= vfmacc (result2, k_vector[1][1], r2c2, vl);	    
			result1= vfmacc (result1, k_vector[1][2], r1c3, vl);
			result2= vfmacc (result2, k_vector[1][2], r2c3, vl);            
			result1= vfmacc (result1, k_vector[2][0], r2c2, vl);
			result2= vfmacc (result2, k_vector[2][0], r3c2, vl);
			result1= vfmacc (result1, k_vector[2][1], r2c2, vl);
			result2= vfmacc (result2, k_vector[2][1], r3c2, vl);
			result1= vfmacc (result1, k_vector[2][2], r2c3, vl);
			vse64(Boutp, result1, vl); //Store result into output B matrix
			result2= vfmacc (result2, k_vector[2][2], r3c3, vl);
			Boutp += size;    
			vse64(Boutp, result2, vl); //Store result into output B matrix 

            	}


            Boutpx += vl;
        }

 #if print_cycles == 1
    uint64_t cycles = rdcycle() - c0;
    uint64_t insts = rdinstret() - i0;
#endif
    TIME_GET(&end);

#if print_cycles == 1
    printf("conv_dpfp unroll=6\n");
    printf("Output pixels %d\n",size*size);
    printf("Cycles per pixel *100 %ld\n", cycles*100/(size*size));
    printf("MACs per cycle %ld\n", (size*size*9)/cycles);
    printf("cycles %ld; instret %ld\n\n",cycles, insts);
 #endif 

    //free memory
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_trans_spfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    size_t i, j, k,m;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    SPFP_TYPE *A = aligned_alloc(4096, sizeof(SPFP_TYPE) * (size * size));
    SPFP_TYPE *B = aligned_alloc(4096, sizeof(SPFP_TYPE) * (size * size));

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
#else
        A[i] = rand();
#endif
        B[i] = 0;
    }

    TIME_GET(&start);

	#define opVectorType        vfloat32m1_t
	#define opVectorStrideLoad  vlse32_v_f32m1
	#define opVectorVlsseg8     vlsseg8e32_v_f32m1
    #define opVectorVlsseg4     vlsseg4e32_v_f32m1
    #define opVectorVlsseg2     vlsseg2e32_v_f32m1
	#define opVectorSetup       vsetvl_e32m1
    #define opVectorStore       vse32_v_f32m1

    size_t vl;

    SPFP_TYPE *a_offset = A;
    SPFP_TYPE *b_offset = B;
    SPFP_TYPE *a_offset1;
    SPFP_TYPE *b_offset1;
    opVectorType v0, v1, v2, v3, v4, v5, v6, v7;
    for (j = size; j > 0; j -= vl)
    {
        vl = opVectorSetup(j);
        a_offset1 = a_offset;
        b_offset1 = b_offset;

        a_offset += vl * size;
        b_offset += vl;
        for (i = 0; i < size/8; i++)
        {
            opVectorVlsseg8(&v0, &v1, &v2, &v3, &v4, &v5, &v6, &v7, a_offset1, size * sizeof(SPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);
            opVectorStore(b_offset1 + size * 4, v4, vl);
            opVectorStore(b_offset1 + size * 5, v5, vl);
            opVectorStore(b_offset1 + size * 6, v6, vl);
            opVectorStore(b_offset1 + size * 7, v7, vl);
            a_offset1 += 8;
            b_offset1 += size * 8;
        }
        if (size & 4)
        {
            opVectorVlsseg4(&v0, &v1, &v2, &v3, a_offset1, size * sizeof(SPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);

            a_offset1 += 4;
            b_offset1 += size * 4;
        }
        if (size & 2)
        {
            opVectorVlsseg2(&v0, &v1, a_offset1, size * sizeof(SPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
        
            a_offset1 += 2;
            b_offset1 += size * 2;
        }
        if (size & 1)
        {
            v0 = opVectorStrideLoad(a_offset1, size * sizeof(SPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
        
            a_offset1++;
            b_offset1 += size;
        }
    }
    
    TIME_GET(&end);

    free(A);
    free(B);

#undef opVectorType        
#undef opVectorStrideLoad  
#undef opVectorVlsseg8     
#undef opVectorVlsseg4     
#undef opVectorVlsseg2     
#undef opVectorSetup       
#undef opVectorStore       

    return TIME_RUNTIME(start, end);
}

double matrix_trans_int8(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    size_t i, j, k,m;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    INT8_TYPE *A = aligned_alloc(4096, sizeof(INT8_TYPE) * (size * size));
    INT8_TYPE *B = aligned_alloc(4096, sizeof(INT8_TYPE) * (size * size));

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
#else
        A[i] = rand();
#endif
        B[i] = 0;
    }

    TIME_GET(&start);

#define opVectorType        vint8m1_t
#define opVectorStrideLoad  vlse8_v_i8m1
#define opVectorVlsseg8     vlsseg8e8_v_i8m1
#define opVectorVlsseg4     vlsseg4e8_v_i8m1
#define opVectorVlsseg2     vlsseg2e8_v_i8m1
#define opVectorSetup       vsetvl_e8m1
#define opVectorStore       vse8_v_i8m1

    size_t vl;

    INT8_TYPE *a_offset = A;
    INT8_TYPE *b_offset = B;
    INT8_TYPE *a_offset1;
    INT8_TYPE *b_offset1;
    opVectorType v0, v1, v2, v3, v4, v5, v6, v7;
    for (j = size; j > 0; j -= vl)
    {
        vl = opVectorSetup(j);
        a_offset1 = a_offset;
        b_offset1 = b_offset;

        a_offset += vl * size;
        b_offset += vl;
        for (i = 0; i < size/8; i++)
        {
            opVectorVlsseg8(&v0, &v1, &v2, &v3, &v4, &v5, &v6, &v7, a_offset1, size * sizeof(INT8_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);
            opVectorStore(b_offset1 + size * 4, v4, vl);
            opVectorStore(b_offset1 + size * 5, v5, vl);
            opVectorStore(b_offset1 + size * 6, v6, vl);
            opVectorStore(b_offset1 + size * 7, v7, vl);
            a_offset1 += 8;
            b_offset1 += size * 8;
        }
        if (size & 4)
        {
            opVectorVlsseg4(&v0, &v1, &v2, &v3, a_offset1, size * sizeof(INT8_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);

            a_offset1 += 4;
            b_offset1 += size * 4;
        }
        if (size & 2)
        {
            opVectorVlsseg2(&v0, &v1, a_offset1, size * sizeof(INT8_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
        
            a_offset1 += 2;
            b_offset1 += size * 2;
        }
        if (size & 1)
        {
            v0 = opVectorStrideLoad(a_offset1, size * sizeof(INT8_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
        
            a_offset1++;
            b_offset1 += size;
        }
    }
    
    TIME_GET(&end);

    free(A);
    free(B);

#undef opVectorType        
#undef opVectorStrideLoad  
#undef opVectorVlsseg8     
#undef opVectorVlsseg4     
#undef opVectorVlsseg2     
#undef opVectorSetup       
#undef opVectorStore       


    return TIME_RUNTIME(start, end);
}

double matrix_trans_int16(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    size_t i, j, k,m;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    INT16_TYPE *A = aligned_alloc(4096, sizeof(INT16_TYPE) * (size * size));
    INT16_TYPE *B = aligned_alloc(4096, sizeof(INT16_TYPE) * (size * size));

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
#else
        A[i] = rand();
#endif
        B[i] = 0;
    }

    TIME_GET(&start);

    #define opVectorType        vint16m1_t
    #define opVectorStrideLoad  vlse16_v_i16m1
    #define opVectorVlsseg8     vlsseg8e16_v_i16m1
    #define opVectorVlsseg4     vlsseg4e16_v_i16m1
    #define opVectorVlsseg2     vlsseg2e16_v_i16m1
    #define opVectorSetup       vsetvl_e16m1
    #define opVectorStore       vse16_v_i16m1
    
    size_t vl;

    INT16_TYPE *a_offset = A;
    INT16_TYPE *b_offset = B;
    INT16_TYPE *a_offset1;
    INT16_TYPE *b_offset1;
    opVectorType v0, v1, v2, v3, v4, v5, v6, v7;
    for (j = size; j > 0; j -= vl)
    {
        vl = opVectorSetup(j);
        a_offset1 = a_offset;
        b_offset1 = b_offset;

        a_offset += vl * size;
        b_offset += vl;
        for (i = 0; i < size/8; i++)
        {
            opVectorVlsseg8(&v0, &v1, &v2, &v3, &v4, &v5, &v6, &v7, a_offset1, size * sizeof(INT16_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);
            opVectorStore(b_offset1 + size * 4, v4, vl);
            opVectorStore(b_offset1 + size * 5, v5, vl);
            opVectorStore(b_offset1 + size * 6, v6, vl);
            opVectorStore(b_offset1 + size * 7, v7, vl);
            a_offset1 += 8;
            b_offset1 += size * 8;
        }
        if (size & 4)
        {
            opVectorVlsseg4(&v0, &v1, &v2, &v3, a_offset1, size * sizeof(INT16_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);

            a_offset1 += 4;
            b_offset1 += size * 4;
        }
        if (size & 2)
        {
            opVectorVlsseg2(&v0, &v1, a_offset1, size * sizeof(INT16_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
        
            a_offset1 += 2;
            b_offset1 += size * 2;
        }
        if (size & 1)
        {
            v0 = opVectorStrideLoad(a_offset1, size * sizeof(INT16_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
        
            a_offset1++;
            b_offset1 += size;
        }
    }
    
    TIME_GET(&end);

    free(A);
    free(B);

#undef opVectorType        
#undef opVectorStrideLoad  
#undef opVectorVlsseg8     
#undef opVectorVlsseg4     
#undef opVectorVlsseg2     
#undef opVectorSetup       
#undef opVectorStore       

    return TIME_RUNTIME(start, end);
}

double matrix_trans_int32(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    size_t i, j, k,m;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    INT32_TYPE *A = aligned_alloc(4096, sizeof(INT32_TYPE) * (size * size));
    INT32_TYPE *B = aligned_alloc(4096, sizeof(INT32_TYPE) * (size * size));

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
#else
        A[i] = rand();
#endif
        B[i] = 0;
    }

    TIME_GET(&start);

#define opVectorType        vint32m1_t
#define opVectorStrideLoad  vlse32_v_i32m1
#define opVectorVlsseg8     vlsseg8e32_v_i32m1
#define opVectorVlsseg4     vlsseg4e32_v_i32m1
#define opVectorVlsseg2     vlsseg2e32_v_i32m1
#define opVectorSetup       vsetvl_e32m1
#define opVectorStore       vse32_v_i32m1

    size_t vl;

    INT32_TYPE *a_offset = A;
    INT32_TYPE *b_offset = B;
    INT32_TYPE *a_offset1;
    INT32_TYPE *b_offset1;
    opVectorType v0, v1, v2, v3, v4, v5, v6, v7;
    for (j = size; j > 0; j -= vl)
    {
        vl = opVectorSetup(j);
        a_offset1 = a_offset;
        b_offset1 = b_offset;

        a_offset += vl * size;
        b_offset += vl;
        for (i = 0; i < size/8; i++)
        {
            opVectorVlsseg8(&v0, &v1, &v2, &v3, &v4, &v5, &v6, &v7, a_offset1, size * sizeof(INT32_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);
            opVectorStore(b_offset1 + size * 4, v4, vl);
            opVectorStore(b_offset1 + size * 5, v5, vl);
            opVectorStore(b_offset1 + size * 6, v6, vl);
            opVectorStore(b_offset1 + size * 7, v7, vl);
            a_offset1 += 8;
            b_offset1 += size * 8;
        }
        if (size & 4)
        {
            opVectorVlsseg4(&v0, &v1, &v2, &v3, a_offset1, size * sizeof(INT32_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);

            a_offset1 += 4;
            b_offset1 += size * 4;
        }
        if (size & 2)
        {
            opVectorVlsseg2(&v0, &v1, a_offset1, size * sizeof(INT32_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
        
            a_offset1 += 2;
            b_offset1 += size * 2;
        }
        if (size & 1)
        {
            v0 = opVectorStrideLoad(a_offset1, size * sizeof(INT32_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
        
            a_offset1++;
            b_offset1 += size;
        }
    }
    
    TIME_GET(&end);

    free(A);
    free(B);

#undef opVectorType        
#undef opVectorStrideLoad  
#undef opVectorVlsseg8     
#undef opVectorVlsseg4     
#undef opVectorVlsseg2     
#undef opVectorSetup       
#undef opVectorStore       

    return TIME_RUNTIME(start, end);
}

double matrix_trans_dpfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    size_t i, j, k,m;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    DPFP_TYPE *A = aligned_alloc(4096, sizeof(DPFP_TYPE) * (size * size));
    DPFP_TYPE *B = aligned_alloc(4096, sizeof(DPFP_TYPE) * (size * size));

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
#else
        A[i] = rand();
#endif
        B[i] = 0;
    }

    TIME_GET(&start);
#define opVectorType        vfloat64m1_t
#define opVectorStrideLoad  vlse64_v_f64m1
#define opVectorVlsseg8     vlsseg8e64_v_f64m1
#define opVectorVlsseg4     vlsseg4e64_v_f64m1
#define opVectorVlsseg2     vlsseg2e64_v_f64m1
#define opVectorSetup       vsetvl_e64m1
#define opVectorStore       vse64_v_f64m1

    size_t vl;

    DPFP_TYPE *a_offset = A;
    DPFP_TYPE *b_offset = B;
    DPFP_TYPE *a_offset1;
    DPFP_TYPE *b_offset1;
    opVectorType v0, v1, v2, v3, v4, v5, v6, v7;
    for (j = size; j > 0; j -= vl)
    {
        vl = opVectorSetup(j);
        a_offset1 = a_offset;
        b_offset1 = b_offset;

        a_offset += vl * size;
        b_offset += vl;
        for (i = 0; i < size/8; i++)
        {
            opVectorVlsseg8(&v0, &v1, &v2, &v3, &v4, &v5, &v6, &v7, a_offset1, size * sizeof(DPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);
            opVectorStore(b_offset1 + size * 4, v4, vl);
            opVectorStore(b_offset1 + size * 5, v5, vl);
            opVectorStore(b_offset1 + size * 6, v6, vl);
            opVectorStore(b_offset1 + size * 7, v7, vl);
            a_offset1 += 8;
            b_offset1 += size * 8;
        }
        if (size & 4)
        {
            opVectorVlsseg4(&v0, &v1, &v2, &v3, a_offset1, size * sizeof(DPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
            opVectorStore(b_offset1 + size * 2, v2, vl);
            opVectorStore(b_offset1 + size * 3, v3, vl);

            a_offset1 += 4;
            b_offset1 += size * 4;
        }
        if (size & 2)
        {
            opVectorVlsseg2(&v0, &v1, a_offset1, size * sizeof(DPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
            opVectorStore(b_offset1 + size,     v1, vl);
        
            a_offset1 += 2;
            b_offset1 += size * 2;
        }
        if (size & 1)
        {
            v0 = opVectorStrideLoad(a_offset1, size * sizeof(DPFP_TYPE), vl);
            opVectorStore(b_offset1,            v0, vl);
        
            a_offset1++;
            b_offset1 += size;
        }
    }
    
    TIME_GET(&end);

    free(A);
    free(B);

#undef opVectorType        
#undef opVectorStrideLoad  
#undef opVectorVlsseg8     
#undef opVectorVlsseg4     
#undef opVectorVlsseg2     
#undef opVectorSetup       
#undef opVectorStore       

    return TIME_RUNTIME(start, end);
}

double keplers_eq_int8(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i;
    srand(SEED);
    TIME_STRUCT start, end;
    //set threshold and scale factor
    INT8_TYPE threshold = 32;
    INT8_TYPE scale = pow(2, 4);
    //allocate memory for vectors of values used for convergences
    INT8_TYPE *e = malloc(sizeof(INT8_TYPE) * size);
    INT8_TYPE *M = malloc(sizeof(INT8_TYPE) * size);
    INT8_TYPE *E = malloc(sizeof(INT8_TYPE) * size);
    INT8_TYPE *Eold = malloc(sizeof(INT8_TYPE) * size);

    //initialize eccentricities to 1
    //initialize mean anomalies to random values in range 0<=E<=2pi
    //and multiply mean anomalies by scale factor
    //initialize eccentric anomalies to mean anomalies as starting point
    //initialize Eold vector to zeros
    for (i = 0; i < size; i++)
    {
        e[i] = 1;
        M[i] = 0;

        while (M[i] == 0)
        {
            M[i] = scale * 2 * M_PI * rand() / (INT16_TYPE)RAND_MAX;
        }

        E[i] = M[i];
        Eold[i] = 0;
    }

    //serial operation
    if (threads == 1)
    {
        //start timer
        TIME_GET(&start);

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (abs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }
    //parallel operation
    else
    {
        //start timer
        TIME_GET(&start);
        //parallelize with OpenMP
        #pragma omp parallel for num_threads(threads) private(i)

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (abs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }

    //free memory
    free(Eold);
    free(E);
    free(M);
    free(e);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double keplers_eq_int16(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i;
    srand(SEED);
    TIME_STRUCT start, end;
    //set threshold and scale factor
    INT16_TYPE threshold = 64;
    INT16_TYPE scale = pow(2, 8);
    //allocate memory for vectors of values used for convergences
    INT16_TYPE *e = malloc(sizeof(INT16_TYPE) * size);
    INT16_TYPE *M = malloc(sizeof(INT16_TYPE) * size);
    INT16_TYPE *E = malloc(sizeof(INT16_TYPE) * size);
    INT16_TYPE *Eold = malloc(sizeof(INT16_TYPE) * size);

    //initialize eccentricities to 1
    //initialize mean anomalies to random values in range 0<=E<=2pi
    //and multiply mean anomalies by scale factor
    //initialize eccentric anomalies to mean anomalies as starting point
    //initialize Eold vector to zeros
    for (i = 0; i < size; i++)
    {
        e[i] = 1;
        M[i] = 0;

        while (M[i] == 0)
        {
            M[i] = scale * 2 * M_PI * rand() / (INT16_TYPE)RAND_MAX;
        }

        E[i] = M[i];
        Eold[i] = 0;
    }

    //serial operation
    if (threads == 1)
    {
        //start timer
        TIME_GET(&start);

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (abs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }
    //parallel operation
    else
    {
        //start timer
        TIME_GET(&start);
        //parallelize with OpenMP
        #pragma omp parallel for num_threads(threads) private(i)

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (abs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }

    //free memory
    free(Eold);
    free(E);
    free(M);
    free(e);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double keplers_eq_int32(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i;
    srand(SEED);
    TIME_STRUCT start, end;
    //set threshold and scale factor
    INT32_TYPE threshold = 128;
    INT32_TYPE scale = pow(2, 16);
    //allocate memory for vectors of values used for convergences
    INT32_TYPE *e = malloc(sizeof(INT32_TYPE) * size);
    INT32_TYPE *M = malloc(sizeof(INT32_TYPE) * size);
    INT32_TYPE *E = malloc(sizeof(INT32_TYPE) * size);
    INT32_TYPE *Eold = malloc(sizeof(INT32_TYPE) * size);

    //initialize eccentricities to 1
    //initialize mean anomalies to random values in range 0<=E<=2pi
    //and multiply mean anomalies by scale factor
    //initialize eccentric anomalies to mean anomalies as starting point
    //initialize Eold vector to zeros
    for (i = 0; i < size; i++)
    {
        e[i] = 1;
        M[i] = scale * 2 * M_PI * rand() / (INT32_TYPE)RAND_MAX;
        E[i] = M[i];
        Eold[i] = 0;
    }

    //serial operation
    if (threads == 1)
    {
        //start timer
        TIME_GET(&start);

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (abs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }
    //parallel operation
    else
    {
        //start timer
        TIME_GET(&start);
        //parallelize with OpenMP
        #pragma omp parallel for num_threads(threads) private(i)

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (abs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }

    //free memory
    free(Eold);
    free(E);
    free(M);
    free(e);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double keplers_eq_spfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i;
    srand(SEED);
    TIME_STRUCT start, end;
    //set threshold
    SPFP_TYPE threshold = 1E-6;
    //allocate memory for vectors of values used for convergences
    SPFP_TYPE *e = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *M = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *E = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *Eold = malloc(sizeof(SPFP_TYPE) * size);

    //initialize eccentricities to random values in range 0<=e<=1
    //initialize mean anomalies to random values in range 0<=E<=2pi
    //initialize eccentric anomalies to mean anomalies as starting point
    //initialize Eold vector to zeros
    for (i = 0; i < size; i++)
    {
        e[i] = rand() / (SPFP_TYPE)RAND_MAX;
        M[i] = 2 * M_PI * rand() / (SPFP_TYPE)RAND_MAX;
        E[i] = M[i];
        Eold[i] = 0;
    }

    //serial operation
    if (threads == 1)
    {
        //start timer
        TIME_GET(&start);

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (fabs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }
    //parallel operation
    else
    {
        //start timer
        TIME_GET(&start);
        //parallelize with OpenMP
        #pragma omp parallel for num_threads(threads) private(i)

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (fabs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }

    //free memory
    free(Eold);
    free(E);
    free(M);
    free(e);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double keplers_eq_dpfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i;
    srand(SEED);
    TIME_STRUCT start, end;
    //set threshold
    DPFP_TYPE threshold = 1E-6;
    //allocate memory for vectors of values used for convergences
    DPFP_TYPE *e = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *M = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *E = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *Eold = malloc(sizeof(DPFP_TYPE) * size);

    //initialize eccentricities to random values in range 0<=e<=1
    //initialize mean anomalies to random values in range 0<=E<=2pi
    //initialize eccentric anomalies to mean anomalies as starting point
    //initialize Eold vector to zeros
    for (i = 0; i < size; i++)
    {
        e[i] = rand() / (DPFP_TYPE)RAND_MAX;
        M[i] = 2 * M_PI * rand() / (DPFP_TYPE)RAND_MAX;
        E[i] = M[i];
        Eold[i] = 0;
    }

    //serial operation
    if (threads == 1)
    {
        //start timer
        TIME_GET(&start);

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (fabs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }
    //parallel operation
    else
    {
        //start timer
        TIME_GET(&start);
        //parallelize with OpenMP
        #pragma omp parallel for num_threads(threads) private(i)

        //computations
        for (i = 0; i < size; i++)
        {
            //iterate until convergence is complete
            while (fabs(E[i] - Eold[i]) > threshold)
            {
                Eold[i] = E[i];
                E[i] = E[i] - (E[i] - e[i] * sin(E[i]) - M[i]) / (1 - e[i] * cos(E[i]));
            }
        }

        //end timer
        TIME_GET(&end);
    }

    //free memory
    free(Eold);
    free(E);
    free(M);
    free(e);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double cw_eq_spfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i;
    srand(SEED);
    TIME_STRUCT start, end;

    //allocate memory for vectors of values
    SPFP_TYPE *x0 = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *y0 = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *z0 = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *xhat0 = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *yhat0 = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *zhat0 = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *t = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *x = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *y = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *z = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *xhat = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *yhat = malloc(sizeof(SPFP_TYPE) * size);
    SPFP_TYPE *zhat = malloc(sizeof(SPFP_TYPE) * size);

    //initialize
    SPFP_TYPE n = 0.0011;
    SPFP_TYPE nr = 1 / n;
    SPFP_TYPE n3 = 3 * n;
    SPFP_TYPE n6 = 6 * n;
    SPFP_TYPE nt = 0;
    SPFP_TYPE sinnt = 0;
    SPFP_TYPE cosnt = 0;

    for (i = 0; i < size; i++)
    {
        x0[i] = rand() / (SPFP_TYPE)RAND_MAX;
        y0[i] = rand() / (SPFP_TYPE)RAND_MAX;
        z0[i] = rand() / (SPFP_TYPE)RAND_MAX;
        xhat0[i] = rand() / (SPFP_TYPE)RAND_MAX;
        yhat0[i] = rand() / (SPFP_TYPE)RAND_MAX;
        zhat0[i] = rand() / (SPFP_TYPE)RAND_MAX;
        t[i] = rand() / (SPFP_TYPE)RAND_MAX;
        x[i] = 0;
        y[i] = 0;
        z[i] = 0;
        xhat[i] = 0;
        yhat[i] = 0;
        zhat[i] = 0;
    }

    //serial operation
    if (threads == 1)
    {
        //start timer
        TIME_GET(&start);

        //computations
        for (i = 0; i < size; i++)
        {
            nt = n * t[i];
            sinnt = sin(nt);
            cosnt = cos(nt);
            x[i] = x0[i] * ((4 - 3 * cosnt) + sinnt * nr + (2 - 2 * cosnt) * nr);
            y[i] = y0[i] * (6 * (sinnt - nt) + 1 - (2 - 2 * cosnt) * nr + (4 * sinnt - 3 * nt) * nr);
            z[i] = z0[i] * (cosnt + sinnt * nr);
            xhat[i] = xhat0[i] * (n3 * sinnt + cosnt + 2 * sinnt);
            yhat[i] = yhat0[i] * (-n6 * (1 - cosnt) - 2 * sinnt + (4 * cosnt - 3));
            zhat[i] = zhat0[i] * (-n * sinnt + cosnt);
        }

        //end timer
        TIME_GET(&end);
    }
    //parallel operation
    else
    {
        //start timer
        TIME_GET(&start);
        //parallelize with OpenMP
        #pragma omp parallel for num_threads(threads) private(i)

        //computations
        for (i = 0; i < size; i++)
        {
            nt = n * t[i];
            sinnt = sin(nt);
            cosnt = cos(nt);
            x[i] = x0[i] * ((4 - 3 * cosnt) + sinnt * nr + (2 - 2 * cosnt) * nr);
            y[i] = y0[i] * (6 * (sinnt - nt) + 1 - (2 - 2 * cosnt) * nr + (4 * sinnt - 3 * nt) * nr);
            z[i] = z0[i] * (cosnt + sinnt * nr);
            xhat[i] = xhat0[i] * (n3 * sinnt + cosnt + 2 * sinnt);
            yhat[i] = yhat0[i] * (-n6 * (1 - cosnt) - 2 * sinnt + (4 * cosnt - 3));
            zhat[i] = zhat0[i] * (-n * sinnt + cosnt);
        }

        //end timer
        TIME_GET(&end);
    }

    //free memory
    free(x0);
    free(y0);
    free(z0);
    free(xhat0);
    free(yhat0);
    free(zhat0);
    free(x);
    free(y);
    free(z);
    free(xhat);
    free(yhat);
    free(zhat);
    free(t);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double cw_eq_dpfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for vectors of values
    DPFP_TYPE *x0 = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *y0 = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *z0 = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *xhat0 = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *yhat0 = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *zhat0 = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *t = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *x = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *y = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *z = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *xhat = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *yhat = malloc(sizeof(DPFP_TYPE) * size);
    DPFP_TYPE *zhat = malloc(sizeof(DPFP_TYPE) * size);
    //initialize
    DPFP_TYPE n = 0.0011;
    DPFP_TYPE nr = 1 / n;
    DPFP_TYPE n3 = 3 * n;
    DPFP_TYPE n6 = 6 * n;
    DPFP_TYPE nt = 0;
    DPFP_TYPE sinnt = 0;
    DPFP_TYPE cosnt = 0;

    for (i = 0; i < size; i++)
    {
        x0[i] = rand() / (DPFP_TYPE)RAND_MAX;
        y0[i] = rand() / (DPFP_TYPE)RAND_MAX;
        z0[i] = rand() / (DPFP_TYPE)RAND_MAX;
        xhat0[i] = rand() / (DPFP_TYPE)RAND_MAX;
        yhat0[i] = rand() / (DPFP_TYPE)RAND_MAX;
        zhat0[i] = rand() / (DPFP_TYPE)RAND_MAX;
        t[i] = rand() / (DPFP_TYPE)RAND_MAX;
        x[i] = 0;
        y[i] = 0;
        z[i] = 0;
        xhat[i] = 0;
        yhat[i] = 0;
        zhat[i] = 0;
    }

    //serial operation
    if (threads == 1)
    {
        //start timer
        TIME_GET(&start);

        //computations
        for (i = 0; i < size; i++)
        {
            nt = n * t[i];
            sinnt = sin(nt);
            cosnt = cos(nt);
            x[i] = x0[i] * ((4 - 3 * cosnt) + sinnt * nr + (2 - 2 * cosnt) * nr);
            y[i] = y0[i] * (6 * (sinnt - nt) + 1 - (2 - 2 * cosnt) * nr + (4 * sinnt - 3 * nt) * nr);
            z[i] = z0[i] * (cosnt + sinnt * nr);
            xhat[i] = xhat0[i] * (n3 * sinnt + cosnt + 2 * sinnt);
            yhat[i] = yhat0[i] * (-n6 * (1 - cosnt) - 2 * sinnt + (4 * cosnt - 3));
            zhat[i] = zhat0[i] * (-n * sinnt + cosnt);
        }

        //end timer
        TIME_GET(&end);
    }
    //parallel operation
    else
    {
        //start timer
        TIME_GET(&start);
        //parallelize with OpenMP
        #pragma omp parallel for num_threads(threads) private(i)

        //computations
        for (i = 0; i < size; i++)
        {
            nt = n * t[i];
            sinnt = sin(nt);
            cosnt = cos(nt);
            x[i] = x0[i] * ((4 - 3 * cosnt) + sinnt * nr + (2 - 2 * cosnt) * nr);
            y[i] = y0[i] * (6 * (sinnt - nt) + 1 - (2 - 2 * cosnt) * nr + (4 * sinnt - 3 * nt) * nr);
            z[i] = z0[i] * (cosnt + sinnt * nr);
            xhat[i] = xhat0[i] * (n3 * sinnt + cosnt + 2 * sinnt);
            yhat[i] = yhat0[i] * (-n6 * (1 - cosnt) - 2 * sinnt + (4 * cosnt - 3));
            zhat[i] = zhat0[i] * (-n * sinnt + cosnt);
        }

        //end timer
        TIME_GET(&end);
    }

    //free memory
    free(x0);
    free(y0);
    free(z0);
    free(xhat0);
    free(yhat0);
    free(zhat0);
    free(x);
    free(y);
    free(z);
    free(xhat);
    free(yhat);
    free(zhat);
    free(t);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_mult_blas_spfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i, j, k;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    SPFP_TYPE *A = malloc(sizeof(SPFP_TYPE) * (size * size));
    SPFP_TYPE *B = malloc(sizeof(SPFP_TYPE) * (size * size));
    SPFP_TYPE *C = malloc(sizeof(SPFP_TYPE) * (size * size));

    //initialize input matrices to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
        C[i] = 0;
    }

    //start timer
    TIME_GET(&start);
    //computation
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1, A, size, B, size, 1, C, size);
    //end timer
    TIME_GET(&end);
    //free memory
    free(C);
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_mult_blas_dpfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i, j, k;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    DPFP_TYPE *A = malloc(sizeof(DPFP_TYPE) * (size * size));
    DPFP_TYPE *B = malloc(sizeof(DPFP_TYPE) * (size * size));
    DPFP_TYPE *C = malloc(sizeof(DPFP_TYPE) * (size * size));

    //initialize input matrices to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
        C[i] = 0;
    }

    //start timer
    TIME_GET(&start);
    //computation
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1, A, size, B, size, 1, C, size);
    //end timer
    TIME_GET(&end);
    //free memory
    free(C);
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_add_blas_spfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i, j, k;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    SPFP_TYPE *A = malloc(sizeof(SPFP_TYPE) * (size * size));
    SPFP_TYPE *B = malloc(sizeof(SPFP_TYPE) * (size * size));

    //initialize input matrices to random numbers
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
    }

    //serial operation
    //start timer
    TIME_GET(&start);
    //computation
    //B=A+B
    cblas_saxpy(size * size, 1, A, 1, B, 1);

    //end timer
    TIME_GET(&end);
    //free memory
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_add_blas_dpfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i, j, k;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    DPFP_TYPE *A = malloc(sizeof(DPFP_TYPE) * (size * size));
    DPFP_TYPE *B = malloc(sizeof(DPFP_TYPE) * (size * size));

    //initialize input matrices to random numbers
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
        B[i] = i;
#else
        A[i] = rand();
        B[i] = rand();
#endif
    }

    //serial operation
    //start timer
    TIME_GET(&start);
    //computation
    //B=A+B
    cblas_daxpy(size * size, 1, A, 1, B, 1);
    //end timer
    TIME_GET(&end);
    //free memory
    free(B);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_conv_blas_spfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i, m, n, mm, nn, ii, j, jj, vecsize;
    int kSize = 3;
    int kCenter = kSize / 2;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    SPFP_TYPE *A = malloc(sizeof(SPFP_TYPE) * (size * size));
    SPFP_TYPE *k = malloc(sizeof(SPFP_TYPE) * (kSize * kSize));
    SPFP_TYPE *B = malloc(sizeof(SPFP_TYPE) * (size * size));

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
#else
        A[i] = rand();
#endif
        B[i] = 0;
    }

    //initialize convolution kernel to 3x3 Sobel filter
    k[0] = -1;
    k[1] = -2;
    k[2] = -1;
    k[3] = 0;
    k[4] = 0;
    k[5] = 0;
    k[6] = 1;
    k[7] = 2;
    k[8] = 1;
    //serial operation
    //start timer
    TIME_GET(&start);

    //computation
    for (i = 0; i < size; ++i)
    {
        for (m = 0; m < kSize; ++m)
        {
            mm = kSize - m - 1;
            ii = i + (m - kCenter);

            if (ii >= 0 && ii < size)
            {
                for (n = 0; n < kSize; ++n)
                {
                    //Compute the starting column index for A,B,&C & vector len
                    nn = kSize - n - 1;
                    j = kCenter - n;
                    jj = j < 0 ? 1 : 0;
                    vecsize = j == 0 ? size : size - 1;
                    j = j > 0 ? j : 0;
                    //have blas loop over the image and compute the convolution for the current row
                    cblas_saxpy(vecsize, k[nn + mm * kSize], &A[ii * size + jj], 1, &B[i * size + j], 1);
                }
            }
        }
    }

    //end timer
    TIME_GET(&end);
    //free memory
    free(B);
    free(k);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

double matrix_conv_blas_dpfp(int size, int threads)
{
    //initialize index variables, random number generator, and timer
    int i, m, n, mm, nn, ii, j, jj, vecsize;
    int kSize = 3;
    int kCenter = kSize / 2;
    srand(SEED);
    TIME_STRUCT start, end;
    //allocate memory for matrices
    DPFP_TYPE *A = malloc(sizeof(DPFP_TYPE) * (size * size));
    DPFP_TYPE *k = malloc(sizeof(DPFP_TYPE) * (kSize * kSize));
    DPFP_TYPE *B = malloc(sizeof(DPFP_TYPE) * (size * size));

    //initialize input matrix to random numbers
    //initialize output matrix to zeros
    for (i = 0; i < (size * size); i++)
    {
#ifdef FAST_INIT // speedup for emulation
        A[i] = i;
#else
        A[i] = rand();
#endif
        B[i] = 0;
    }

    //initialize convolution kernel to 3x3 Sobel filter
    k[0] = -1;
    k[1] = -2;
    k[2] = -1;
    k[3] = 0;
    k[4] = 0;
    k[5] = 0;
    k[6] = 1;
    k[7] = 2;
    k[8] = 1;
    //serial operation
    //start timer
    TIME_GET(&start);

    //computation
    for (i = 0; i < size; ++i)
    {
        for (m = 0; m < kSize; ++m)
        {
            mm = kSize - m - 1;
            ii = i + (m - kCenter);

            if (ii >= 0 && ii < size)
            {
                for (n = 0; n < kSize; ++n)
                {
                    //Compute the starting column index for A,B,&C & vector len
                    nn = kSize - n - 1;
                    j = kCenter - n;
                    jj = j < 0 ? 1 : 0;
                    vecsize = j == 0 ? size : size - 1;
                    j = j > 0 ? j : 0;
                    //have blas loop over the image and compute the convolution for the current row
                    cblas_daxpy(vecsize, k[nn + mm * kSize], &A[ii * size + jj], 1, &B[i * size + j], 1);
                }
            }
        }
    }

    //end timer
    TIME_GET(&end);
    //free memory
    free(B);
    free(k);
    free(A);
    //compute and return runtime
    return TIME_RUNTIME(start, end);
}

