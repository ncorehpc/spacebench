# started on Tue Nov  9 13:21:28 2021


 Performance counter stats for './spacebench 5 4 1024 16 1000' (5 runs):

         219097275      instructions:u                                                ( +-  0.02% )
          86655588      L1-dcache-loads:u                                             ( +-  0.01% )
            325189      L1-dcache-load-misses:u   #    0.38% of all L1-dcache hits    ( +-  2.19% )
         134608910      L1-icache-loads:u                                             ( +-  0.04% )
            184467      L1-icache-load-misses:u   #    0.14% of all L1-icache hits    ( +-  1.16% )

           0.55563 +- 0.00468 seconds time elapsed  ( +-  0.84% )

