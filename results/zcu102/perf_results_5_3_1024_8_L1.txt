# started on Tue Nov  9 13:20:49 2021


 Performance counter stats for './spacebench 5 3 1024 8 1000' (5 runs):

         125317552      instructions:u                                                ( +-  0.06% )
          48555660      L1-dcache-loads:u                                             ( +-  0.03% )
            303446      L1-dcache-load-misses:u   #    0.62% of all L1-dcache hits    ( +-  8.15% )
          75677106      L1-icache-loads:u                                             ( +-  0.06% )
            179967      L1-icache-load-misses:u   #    0.24% of all L1-icache hits    ( +-  2.39% )

           0.28580 +- 0.00442 seconds time elapsed  ( +-  1.55% )

