# started on Tue Nov  9 08:54:31 2021


 Performance counter stats for './spacebench 4 3 2048 16 100' (5 runs):

       33505621464      instructions:u                                                ( +-  0.00% )
       15392194265      L1-dcache-loads:u                                             ( +-  0.00% )
          54198083      L1-dcache-load-misses:u   #    0.35% of all L1-dcache hits    ( +-  0.30% )
       20445406763      L1-icache-loads:u                                             ( +-  0.01% )
           1364364      L1-icache-load-misses:u   #    0.01% of all L1-icache hits    ( +- 55.74% )

           47.6093 +- 0.0208 seconds time elapsed  ( +-  0.04% )

