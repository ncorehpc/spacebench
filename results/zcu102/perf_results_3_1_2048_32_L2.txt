# started on Tue Nov  9 04:52:26 2021


 Performance counter stats for './spacebench 3 1 2048 32 100' (5 runs):

          28062235      armv8_pmuv3/l2d_cache/u                                       ( +-  0.03% )
           7233279      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.09% )
          18206014      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.03% )
        7694524278      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       14432552328      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

          34.47987 +- 0.00431 seconds time elapsed  ( +-  0.01% )

