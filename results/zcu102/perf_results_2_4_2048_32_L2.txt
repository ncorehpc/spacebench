# started on Tue Nov  9 02:58:35 2021


 Performance counter stats for './spacebench 2 4 2048 32 100' (5 runs):

         188527860      armv8_pmuv3/l2d_cache/u                                       ( +-  0.47% )
          80535290      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.00% )
          65790983      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.06% )
       15106793685      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       28959266566      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           66.8447 +- 0.0153 seconds time elapsed  ( +-  0.02% )

