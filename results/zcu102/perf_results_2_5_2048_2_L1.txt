# started on Tue Nov  9 03:16:57 2021


 Performance counter stats for './spacebench 2 5 2048 2 100' (5 runs):

       64513595924      instructions:u                                                ( +-  0.00% )
       28633212971      L1-dcache-loads:u                                             ( +-  0.00% )
          67090986      L1-dcache-load-misses:u   #    0.23% of all L1-dcache hits    ( +-  1.15% )
       38771536125      L1-icache-loads:u                                             ( +-  0.01% )
           1636596      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 51.50% )

           75.4717 +- 0.0169 seconds time elapsed  ( +-  0.02% )

