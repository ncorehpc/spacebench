# started on Tue Nov  9 05:50:45 2021


 Performance counter stats for './spacebench 3 4 2048 2 100' (5 runs):

         173972864      armv8_pmuv3/l2d_cache/u                                       ( +-  0.22% )
          52676349      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.00% )
          60328616      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.11% )
       10068082442      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       17619056889      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

          41.77866 +- 0.00424 seconds time elapsed  ( +-  0.01% )

