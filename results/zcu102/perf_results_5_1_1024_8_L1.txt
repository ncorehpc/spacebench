# started on Tue Nov  9 11:01:09 2021


 Performance counter stats for './spacebench 5 1 1024 8 1000' (5 runs):

        8318421542      instructions:u                                                ( +-  0.00% )
        3521520880      L1-dcache-loads:u                                             ( +-  0.00% )
            148784      L1-dcache-load-misses:u   #    0.00% of all L1-dcache hits    ( +-  1.30% )
        5185652693      L1-icache-loads:u                                             ( +-  0.00% )
            169862      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +-  2.34% )

           8.75136 +- 0.00431 seconds time elapsed  ( +-  0.05% )

