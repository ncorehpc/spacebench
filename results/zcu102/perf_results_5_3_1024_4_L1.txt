# started on Tue Nov  9 13:20:46 2021


 Performance counter stats for './spacebench 5 3 1024 4 1000' (5 runs):

         211280735      instructions:u                                                ( +-  1.32% )
          64776802      L1-dcache-loads:u                                             ( +-  0.72% )
            261806      L1-dcache-load-misses:u   #    0.40% of all L1-dcache hits    ( +- 25.35% )
         120940108      L1-icache-loads:u                                             ( +-  1.16% )
             66460      L1-icache-load-misses:u   #    0.05% of all L1-icache hits    ( +-  1.58% )

           0.27046 +- 0.00205 seconds time elapsed  ( +-  0.76% )

