# started on Tue Nov  9 10:47:38 2021


 Performance counter stats for './spacebench 4 5 2048 32 100' (5 runs):

       32713974431      instructions:u                                                ( +-  0.00% )
       14289858799      L1-dcache-loads:u                                             ( +-  0.00% )
          34587842      L1-dcache-load-misses:u   #    0.24% of all L1-dcache hits    ( +-  0.41% )
       20276083731      L1-icache-loads:u                                             ( +-  0.00% )
            904214      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 58.22% )

           54.6990 +- 0.0281 seconds time elapsed  ( +-  0.05% )

