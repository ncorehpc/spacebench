# started on Mon Nov  8 22:03:20 2021


 Performance counter stats for './spacebench 1 4 1024 2 10' (5 runs):

       15212846485      instructions:u                                                ( +-  0.00% )
        8837556919      L1-dcache-loads:u                                             ( +-  0.00% )
          22738809      L1-dcache-load-misses:u   #    0.26% of all L1-dcache hits    ( +-  0.52% )
        7841247821      L1-icache-loads:u                                             ( +-  0.00% )
             44463      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 42.79% )

          20.52269 +- 0.00409 seconds time elapsed  ( +-  0.02% )

