# started on Tue Nov  9 13:22:17 2021


 Performance counter stats for './spacebench 5 5 1024 32 1000' (5 runs):

           2152963      armv8_pmuv3/l2d_cache/u                                       ( +-  1.17% )
             13226      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  1.25% )
             37104      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.27% )
          43907469      armv8_pmuv3/ld_retired/u                                      ( +-  0.04% )
          83692527      armv8_pmuv3/mem_access/u                                      ( +-  0.02% )

           0.61630 +- 0.00844 seconds time elapsed  ( +-  1.37% )

