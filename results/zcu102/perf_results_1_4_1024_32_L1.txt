# started on Mon Nov  8 22:12:26 2021


 Performance counter stats for './spacebench 1 4 1024 32 10' (5 runs):

        2454027282      instructions:u                                                ( +-  0.00% )
        1221558288      L1-dcache-loads:u                                             ( +-  0.00% )
           3003067      L1-dcache-load-misses:u   #    0.25% of all L1-dcache hits    ( +-  1.87% )
        1398643625      L1-icache-loads:u                                             ( +-  0.00% )
             46358      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 40.51% )

          11.35087 +- 0.00439 seconds time elapsed  ( +-  0.04% )

