# started on Tue Nov  9 06:06:59 2021


 Performance counter stats for './spacebench 3 4 2048 16 100' (5 runs):

       34480344680      instructions:u                                                ( +-  0.00% )
       14367953624      L1-dcache-loads:u                                             ( +-  0.00% )
           4419066      L1-dcache-load-misses:u   #    0.03% of all L1-dcache hits    ( +-  1.20% )
       21138993354      L1-icache-loads:u                                             ( +-  0.00% )
            651976      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 31.56% )

           38.4641 +- 0.0565 seconds time elapsed  ( +-  0.15% )

