# started on Tue Nov  9 13:20:45 2021


 Performance counter stats for './spacebench 5 3 1024 2 1000' (5 runs):

            445842      armv8_pmuv3/l2d_cache/u                                       ( +- 15.00% )
             11632      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.16% )
              8061      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.16% )
          52163328      armv8_pmuv3/ld_retired/u                                      ( +-  0.22% )
          81814462      armv8_pmuv3/mem_access/u                                      ( +-  0.14% )

          0.324155 +- 0.000735 seconds time elapsed  ( +-  0.23% )

