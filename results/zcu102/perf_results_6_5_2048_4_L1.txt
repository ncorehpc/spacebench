# started on Tue Nov  9 13:24:21 2021


 Performance counter stats for './spacebench 6 5 2048 4 1000' (5 runs):

        1251838282      instructions:u                                                ( +-  0.17% )
         539018529      L1-dcache-loads:u                                             ( +-  0.07% )
           6431768      L1-dcache-load-misses:u   #    1.19% of all L1-dcache hits    ( +-  2.28% )
         745476882      L1-icache-loads:u                                             ( +-  0.14% )
            123203      L1-icache-load-misses:u   #    0.02% of all L1-icache hits    ( +-  7.45% )

           1.46815 +- 0.00261 seconds time elapsed  ( +-  0.18% )

