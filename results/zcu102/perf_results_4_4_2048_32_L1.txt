# started on Tue Nov  9 09:51:51 2021


 Performance counter stats for './spacebench 4 4 2048 32 100' (5 runs):

       33718526058      instructions:u                                                ( +-  0.00% )
       15248679662      L1-dcache-loads:u                                             ( +-  0.00% )
          51867936      L1-dcache-load-misses:u   #    0.34% of all L1-dcache hits    ( +-  0.61% )
       20754110492      L1-icache-loads:u                                             ( +-  0.01% )
           2281076      L1-icache-load-misses:u   #    0.01% of all L1-icache hits    ( +- 38.87% )

           48.4778 +- 0.0112 seconds time elapsed  ( +-  0.02% )

