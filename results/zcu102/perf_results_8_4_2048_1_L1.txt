# started on Tue Nov  9 13:26:37 2021


 Performance counter stats for './spacebench 8 4 2048 1 100' (5 runs):

       63350518350      instructions:u                                                ( +-  0.00% )
       27841822942      L1-dcache-loads:u                                             ( +-  0.00% )
          16614982      L1-dcache-load-misses:u   #    0.06% of all L1-dcache hits    ( +-  0.48% )
       38291156592      L1-icache-loads:u                                             ( +-  0.00% )
           1320618      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 38.76% )

          64.98055 +- 0.00700 seconds time elapsed  ( +-  0.01% )

