# started on Tue Nov  9 00:30:53 2021


 Performance counter stats for './spacebench 2 2 2048 16 100' (5 runs):

       62836754695      instructions:u                                                ( +-  0.00% )
       28119906296      L1-dcache-loads:u                                             ( +-  0.00% )
          17202637      L1-dcache-load-misses:u   #    0.06% of all L1-dcache hits    ( +-  2.58% )
       38230512600      L1-icache-loads:u                                             ( +-  0.00% )
            917735      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 21.51% )

           63.0513 +- 0.0895 seconds time elapsed  ( +-  0.14% )

