# started on Tue Nov  9 00:04:39 2021


 Performance counter stats for './spacebench 2 2 2048 2 100' (5 runs):

         128041277      armv8_pmuv3/l2d_cache/u                                       ( +-  0.26% )
          52602823      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.00% )
          38413702      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.06% )
       15153035640      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       29073756853      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           63.0621 +- 0.0901 seconds time elapsed  ( +-  0.14% )

