# started on Tue Nov  9 05:34:13 2021


 Performance counter stats for './spacebench 3 3 2048 2 100' (5 runs):

         151050921      armv8_pmuv3/l2d_cache/u                                       ( +-  0.02% )
          13868603      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.05% )
         104648891      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.00% )
        1890212860      armv8_pmuv3/ld_retired/u                                      ( +-  0.01% )
        2258229127      armv8_pmuv3/mem_access/u                                      ( +-  0.01% )

           7.93097 +- 0.00271 seconds time elapsed  ( +-  0.03% )

