# started on Mon Nov  8 22:47:27 2021


 Performance counter stats for './spacebench 2 1 2048 1 100' (5 runs):

       62966795581      instructions:u                                                ( +-  0.00% )
       28234123361      L1-dcache-loads:u                                             ( +-  0.00% )
          10195646      L1-dcache-load-misses:u   #    0.04% of all L1-dcache hits    ( +-  1.02% )
       38282746026      L1-icache-loads:u                                             ( +-  0.00% )
            293003      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 28.83% )

          61.45968 +- 0.00249 seconds time elapsed  ( +-  0.00% )

