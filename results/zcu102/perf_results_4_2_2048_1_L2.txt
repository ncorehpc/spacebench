# started on Tue Nov  9 07:44:14 2021


 Performance counter stats for './spacebench 4 2 2048 1 100' (5 runs):

         739305819      armv8_pmuv3/l2d_cache/u                                       ( +-  0.03% )
         197605765      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.00% )
         332618870      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.01% )
        8127091625      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       17827688083      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           43.1123 +- 0.0101 seconds time elapsed  ( +-  0.02% )

