# started on Tue Nov  9 04:55:18 2021


 Performance counter stats for './spacebench 3 2 2048 1 100' (5 runs):

       47036877041      instructions:u                                                ( +-  0.00% )
       18141985271      L1-dcache-loads:u                                             ( +-  0.00% )
          13203675      L1-dcache-load-misses:u   #    0.07% of all L1-dcache hits    ( +-  0.06% )
       28142430658      L1-icache-loads:u                                             ( +-  0.00% )
             71189      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +-  8.79% )

         41.320736 +- 0.000432 seconds time elapsed  ( +-  0.00% )

