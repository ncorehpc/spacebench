# started on Tue Nov  9 08:21:53 2021


 Performance counter stats for './spacebench 4 3 2048 1 100' (5 runs):

       40296540269      instructions:u                                                ( +-  0.00% )
       19716211308      L1-dcache-loads:u                                             ( +-  0.00% )
         113006554      L1-dcache-load-misses:u   #    0.57% of all L1-dcache hits    ( +-  0.24% )
       23977843991      L1-icache-loads:u                                             ( +-  0.01% )
           2295551      L1-icache-load-misses:u   #    0.01% of all L1-icache hits    ( +- 49.06% )

           52.5058 +- 0.0247 seconds time elapsed  ( +-  0.05% )

