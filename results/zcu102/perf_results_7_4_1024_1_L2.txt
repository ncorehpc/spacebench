# started on Tue Nov  9 13:25:34 2021


 Performance counter stats for './spacebench 7 4 1024 1 10' (5 runs):

          56434553      armv8_pmuv3/l2d_cache/u                                       ( +-  0.23% )
          10317936      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  4.07% )
           2576989      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.31% )
         800727738      armv8_pmuv3/ld_retired/u                                      ( +-  0.01% )
        1157692900      armv8_pmuv3/mem_access/u                                      ( +-  0.01% )

          2.663629 +- 0.000211 seconds time elapsed  ( +-  0.01% )

