# started on Tue Nov  9 05:22:49 2021


 Performance counter stats for './spacebench 3 2 2048 16 100' (5 runs):

          59252263      armv8_pmuv3/l2d_cache/u                                       ( +-  0.01% )
          15693083      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.04% )
          38801025      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.03% )
        7838679777      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       14602890894      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           34.9097 +- 0.0162 seconds time elapsed  ( +-  0.05% )

