# started on Tue Nov  9 08:50:33 2021


 Performance counter stats for './spacebench 4 3 2048 8 100' (5 runs):

         401172187      armv8_pmuv3/l2d_cache/u                                       ( +-  0.33% )
         141847526      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.02% )
         161234987      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.08% )
        7695048660      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       16099403013      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           47.4719 +- 0.0289 seconds time elapsed  ( +-  0.06% )

