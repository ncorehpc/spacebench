# started on Tue Nov  9 11:16:49 2021


 Performance counter stats for './spacebench 5 2 1024 1 1000' (5 runs):

            734269      armv8_pmuv3/l2d_cache/u                                       ( +-  7.14% )
             12070      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.43% )
              7129      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  1.03% )
       30139082625      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       56917624779      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

         135.19413 +- 0.00323 seconds time elapsed  ( +-  0.00% )

