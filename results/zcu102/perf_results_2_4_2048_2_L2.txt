# started on Tue Nov  9 02:14:01 2021


 Performance counter stats for './spacebench 2 4 2048 2 100' (5 runs):

         254408650      armv8_pmuv3/l2d_cache/u                                       ( +-  0.38% )
         105162675      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.00% )
          78699805      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.05% )
       15205684198      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       29205581858      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           67.1052 +- 0.0107 seconds time elapsed  ( +-  0.02% )

