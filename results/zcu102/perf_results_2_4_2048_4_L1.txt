# started on Tue Nov  9 02:19:37 2021


 Performance counter stats for './spacebench 2 4 2048 4 100' (5 runs):

       63870318361      instructions:u                                                ( +-  0.00% )
       28236645346      L1-dcache-loads:u                                             ( +-  0.00% )
          33290614      L1-dcache-load-misses:u   #    0.12% of all L1-dcache hits    ( +-  0.38% )
       38354563448      L1-icache-loads:u                                             ( +-  0.01% )
            871708      L1-icache-load-misses:u   #    0.00% of all L1-icache hits    ( +- 41.93% )

          66.78454 +- 0.00972 seconds time elapsed  ( +-  0.01% )

