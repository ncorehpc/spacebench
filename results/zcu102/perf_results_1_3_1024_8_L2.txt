# started on Mon Nov  8 21:43:32 2021


 Performance counter stats for './spacebench 1 3 1024 8 10' (5 runs):

         166050969      armv8_pmuv3/l2d_cache/u                                       ( +-  1.06% )
          58899211      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  3.48% )
           4288726      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.25% )
        4407333048      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
        6117111463      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           31.9513 +- 0.0109 seconds time elapsed  ( +-  0.03% )

