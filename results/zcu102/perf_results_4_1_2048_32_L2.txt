# started on Tue Nov  9 07:37:35 2021


 Performance counter stats for './spacebench 4 1 2048 32 100' (5 runs):

          83194096      armv8_pmuv3/l2d_cache/u                                       ( +-  1.10% )
          28920256      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  0.01% )
          25602322      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.18% )
        7560132378      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
       15558348458      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

          36.68440 +- 0.00796 seconds time elapsed  ( +-  0.02% )

