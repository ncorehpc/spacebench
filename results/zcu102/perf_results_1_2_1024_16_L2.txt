# started on Mon Nov  8 21:15:19 2021


 Performance counter stats for './spacebench 1 2 1024 16 10' (5 runs):

          43423699      armv8_pmuv3/l2d_cache/u                                       ( +-  1.72% )
          13678287      armv8_pmuv3/l2d_cache_refill/u                                     ( +-  5.67% )
           1916296      armv8_pmuv3/l2d_cache_wb/u                                     ( +-  0.27% )
         716100756      armv8_pmuv3/ld_retired/u                                      ( +-  0.00% )
        1414399446      armv8_pmuv3/mem_access/u                                      ( +-  0.00% )

           8.37075 +- 0.00643 seconds time elapsed  ( +-  0.08% )

